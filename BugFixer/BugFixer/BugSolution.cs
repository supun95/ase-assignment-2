﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ColorCode;
using System.IO;

namespace BugFixer
{
    public partial class BugSolution : Form
    {
        /// <summary>
        /// setting up strings to obtain the username and the bugname from the previous from
        /// </summary>
        public string authenticated_user { get; set; }
        public string passed_bug { get; set; }

        /// <summary>
        /// Relevant methods are initialised when the form loads 
        /// </summary>
        /// <param name="passed_bug"></param>
        /// <param name="membername"></param>
        public BugSolution(string passed_bug, string membername)
        {
            InitializeComponent();
            this.authenticated_user = membername;
            this.passed_bug = passed_bug;
            database_connection();
            from_start();
            bug_status();
        }

        /// <summary>
        /// initiates database connection
        /// </summary>
        SqlConnection mySqlConnection;
        public String clicked_bug;

        /// <summary>
        /// database connection is set up to query to the database via the form
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// obtaining relevant data from the database using the passed bug name from the previous form and setting up the form information when the form loads up. Thus, te user will obtain required information straight away.
        /// </summary>
        public void from_start()
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM Bugs WHERE file_name = '" + this.passed_bug + "'";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);
            try
            {
                
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                //populate textboxes accordingly
                while (mySqlDataReader.Read())
                {
                    bug_name.Text = mySqlDataReader["file_name"].ToString();
                    clicked_bug= mySqlDataReader["file_name"].ToString();
                    code.Text = mySqlDataReader["code_type"].ToString();
                    member.Text = mySqlDataReader["fixer_name"].ToString();
                    op.Text = mySqlDataReader["membername"].ToString();
                    os.Text = mySqlDataReader["os"].ToString();
                    date_posted.Text = mySqlDataReader["submit_date"].ToString();
                    Proj.Text = mySqlDataReader["project_name"].ToString();
                    priority.Text = mySqlDataReader["priority"].ToString();
                    bugcode.Text = mySqlDataReader["uploaded_code"].ToString();
                    string bugCode = mySqlDataReader["uploaded_code"].ToString();
                    string colorizedmyCode = new CodeColorizer().Colorize(bugCode, Languages.CSharp);
                    bug_code_col.DocumentText = colorizedmyCode;
                    mod_code.Text = mySqlDataReader["fixed_code"].ToString();
                    string fixCode = mySqlDataReader["fixed_code"].ToString();
                    string colorizedinitCode = new CodeColorizer().Colorize(fixCode, Languages.CSharp);
                    mod_code_col.DocumentText = colorizedinitCode;
                    mod_desc.Text = mySqlDataReader["fix_desc"].ToString();                    
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// allows the user to set the bug status depending on their opinion about the answer for the query
        /// </summary>
        public void bug_status()
        {
            status.Items.Add("Bug is Fixed");
            status.Items.Add("Bug is Not Fixed");
        }

        /// <summary>
        /// checks validity of user input. Error messages will be displayed if the user is not satisfying this scenario
        /// </summary>
        /// <returns></returns>
        public bool validate_info()
        {
            bool returnvalue = true;

            if (
                string.IsNullOrEmpty(status.Text))
            {
                MessageBox.Show("Error: Please specify the bug status");
                returnvalue = false;
            }

            if (status.Text != "Bug is Fixed" && status.Text != "Bug is Not Fixed")
            {
                MessageBox.Show("Error: Please select the status from the given list");
                returnvalue = false;
            }

            if (status.Text == "Bug is Not Fixed" && string.IsNullOrEmpty(reason.Text))
            {
                MessageBox.Show("Error: Please provide a reason why the big is not fixed");
                returnvalue = false;
            }

            return (returnvalue);

        }

        /// <summary>
        /// for the update process of the bug information stored in the database
        /// </summary>
        /// <param name="status"></param>
        /// <param name="bugname"></param>
        /// <param name="commandString"></param>
        public void bugrecordupdate(String bug_status, String file_name, String commandString)
        {
            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// for the update process of the bug record stored in the bugaudit
        /// </summary>
        /// <param name="bugname"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="username"></param>
        /// <param name="fixersname"></param>
        /// <param name="status"></param>
        /// <param name="initialcode"></param>
        /// <param name="finalcode"></param>
        /// <param name="codetype"></param>
        /// <param name="commandString2"></param>
        public void updatebugAudit(String file_name, String description, DateTime date, String membername, String fixers_name, String bug_status, String initial_code, String final_code, String code_type, String commandString2)
        {
            try
            {
                SqlCommand cmdInsert2 = new SqlCommand(commandString2, mySqlConnection);
                cmdInsert2.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@membername", membername);
                cmdInsert2.Parameters.AddWithValue("@fixers_name", fixers_name);
                cmdInsert2.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert2.Parameters.AddWithValue("@initial_code", initial_code);
                cmdInsert2.Parameters.AddWithValue("@final_code", final_code);
                cmdInsert2.Parameters.AddWithValue("@code_type", code_type);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// submission of updated information into database using sql queries 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_query_Click(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            if (validate_info())
            {
                String commandString = "UPDATE Bugs SET bug_status = @bug_status WHERE file_name = @file_name";
                bugrecordupdate(status.Text, this.passed_bug, commandString);
                MessageBox.Show("Your bug status has changed");

                if (status.Text == "Bug is Fixed")
                {
                    String commandString2 = "INSERT INTO BugAudit(file_name, description, date, membername, fixers_name, bug_status, initial_code, final_code, code_type) VALUES (@file_name, @description, @date, @membername, @fixers_name, @bug_status, @initial_code, @final_code, @code_type)";
                    updatebugAudit(this.passed_bug, mod_desc.Text, DateTime.Now, this.authenticated_user, member.Text, "Bug is Fixed", bugcode.Text, mod_code.Text, code.Text, commandString2);
                }
                if (status.Text == "Bug is Not Fixed")
                {
                    String commandString2 = "INSERT INTO BugAudit(file_name, description, date, membername, fixers_name, bug_status, initial_code, final_code, code_type) VALUES (@file_name, @description, @date, @membername, @fixers_name, @bug_status, @initial_code, @final_code, @code_type)";
                    updatebugAudit(this.passed_bug, reason.Text, DateTime.Now, this.authenticated_user, member.Text, "Bug is Not Fixed", bugcode.Text, mod_code.Text, code.Text, commandString2);
                }
                this.Close();

            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// if the user wants to download the code save file dialog will be opened and the user will be able to save the source code in a folder that they desire. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void download_code_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, mod_code.Text);
            }
        }

        /// <summary>
        /// the user can check the history of the bug through the bug audit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_audit_Click(object sender, EventArgs e)
        {
            BugAudit newForm = new BugAudit(clicked_bug, this.authenticated_user);
            newForm.Show();
            this.Close();
        }
    }
}
