﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;
using Octokit;


namespace BugFixer
{
    public partial class MainHub : Form
    {
        /// <summary>
        ///  Relevant methods are initialised when the form loads
        /// </summary>
        public MainHub()
        {
            InitializeComponent();
            menuStrip1.Visible = false;
            Logout_button.Visible = false;
            userpass_input.PasswordChar = '*'; 
        }

        /// <summary>
        /// initiates the sql connection 
        /// </summary>
        SqlConnection mySqlConnection;

        public String authenticated_member;
        public string MyProperty { get; set; }

        /// <summary>
        /// users password will be hashed using mdf5 hash in order to secure user privacy
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Md5PasswordHash(string text)
        {
            MD5 md5_password_hash = new MD5CryptoServiceProvider();

            md5_password_hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5_password_hash.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// when the user logs in, relelvant data will be obtained from the database and the screen will be setup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, EventArgs e)
        {
            //string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection.Open();

            if (check_input_data())
            {
                string hashed_password = Md5PasswordHash(userpass_input.Text);
                String exisitng_user_identification = "SELECT COUNT(*) FROM users WHERE user_name = '" + username_input.Text + "' AND user_password = '" + hashed_password + "'";
                SqlCommand cmd_check_user = new SqlCommand(exisitng_user_identification, mySqlConnection);
                int user_identification = (int)cmd_check_user.ExecuteScalar();

                if (user_identification > 0)
                {
                    panel1.Visible = false;
                    menuStrip1.Visible = true;
                    SignUp_Button.Visible = false;
                    authenticated_member = username_input.Text;
                    user_name.Text = authenticated_member;
                    Logout_button.Visible = true;
                }
                else
                {
                    MessageBox.Show("Error: User does not exist");
                }
            }
        }

        /// <summary>
        /// validation of data that the user is inputing. username and password text boxes cannot be empty
        /// </summary>
        /// <returns></returns>
        public bool check_input_data()
        {
            bool returnvalue = true;

            if (
                string.IsNullOrEmpty(username_input.Text)
                || string.IsNullOrEmpty(userpass_input.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                returnvalue = false;
            }

            return (returnvalue);

        }

        /// <summary>
        /// user can sign up to the platform using this button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SignUpButton_Click(object sender, EventArgs e)
        {
            SignUp signupform = new SignUp();
            signupform.Show();
        }

        /// <summary>
        /// the logout process of the system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Logout_button_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            user_name.Text = "Please login";
            username_input.Text = userpass_input.Text = "";
            menuStrip1.Visible = false;
            Logout_button.Visible = false;
            SignUp_Button.Visible = true;
        }

        /// <summary>
        /// opens the report bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void report_bug_Click(object sender, EventArgs e)
        {
            ReportBug reportbugForm = new ReportBug(authenticated_member);
            reportbugForm.Show();
        }

        /// <summary>
        /// opens the create project form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void create_project_Click(object sender, EventArgs e)
        {
            NewProject createprojectForm = new NewProject(authenticated_member);
            createprojectForm.Show();
        }

        /// <summary>
        /// opens the projects form. All the projects that are created by the member can be seen on this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_projects_Click(object sender, EventArgs e)
        {
            MyProjects projectsForm = new MyProjects(authenticated_member);
            projectsForm.Show();
        }

        /// <summary>
        /// opens the list of bugs that requires fixing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fix_bugs_Click(object sender, EventArgs e)
        {
            List buglist = new List(authenticated_member);
            buglist.Show();
        }

        /// <summary>
        /// opens my questions form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_questions_Click(object sender, EventArgs e)
        {
            MyQuestions questionlist = new MyQuestions(authenticated_member);
            questionlist.Show();
        }

        /// <summary>
        /// opens bug solutions list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_answers_Click(object sender, EventArgs e)
        {
            BugSolList bugsollist = new BugSolList(authenticated_member);
            bugsollist.Show();
        }

        /// <summary>
        /// opens up the new project form through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewProject createprojectForm = new NewProject(authenticated_member);
            createprojectForm.Show();

        }

        /// <summary>
        /// opens up the project list through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void projectListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyProjects projectsForm = new MyProjects(authenticated_member);
            projectsForm.Show();
        }

        /// <summary>
        /// opens up the report form through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportBug reportbugForm = new ReportBug(authenticated_member);
            reportbugForm.Show();
        }

        /// <summary>
        /// opens up the bug list form through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bugListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List buglist = new List(authenticated_member);
            buglist.Show();
        }

        /// <summary>
        /// opens up the myquestions form through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myQuestionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyQuestions questionlist = new MyQuestions(authenticated_member);
            questionlist.Show();
        }

        /// <summary>
        /// opens up the myanswers form through the menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myAnswersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugSolList bugsollist = new BugSolList(authenticated_member);
            bugsollist.Show();
        }
    }
}
