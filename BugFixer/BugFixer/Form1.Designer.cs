﻿namespace BugFixer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SignUp_Button = new System.Windows.Forms.Button();
            this.Login_Button = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.UserMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menu2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Logout_button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Password = new System.Windows.Forms.Label();
            this.Username = new System.Windows.Forms.Label();
            this.userpass_input = new System.Windows.Forms.TextBox();
            this.username_input = new System.Windows.Forms.TextBox();
            this.Edit_user_info = new System.Windows.Forms.Button();
            this.user_name = new System.Windows.Forms.Label();
            this.report_bug = new System.Windows.Forms.Button();
            this.create_project = new System.Windows.Forms.Button();
            this.my_projects = new System.Windows.Forms.Button();
            this.fix_bugs = new System.Windows.Forms.Button();
            this.my_questions = new System.Windows.Forms.Button();
            this.my_answers = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(478, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Fixer";
            // 
            // SignUp_Button
            // 
            this.SignUp_Button.Location = new System.Drawing.Point(901, 49);
            this.SignUp_Button.Name = "SignUp_Button";
            this.SignUp_Button.Size = new System.Drawing.Size(88, 35);
            this.SignUp_Button.TabIndex = 1;
            this.SignUp_Button.Text = "Sign Up";
            this.SignUp_Button.UseVisualStyleBackColor = true;
            this.SignUp_Button.Click += new System.EventHandler(this.SignUpButton_Click);
            // 
            // Login_Button
            // 
            this.Login_Button.Location = new System.Drawing.Point(221, 155);
            this.Login_Button.Name = "Login_Button";
            this.Login_Button.Size = new System.Drawing.Size(75, 35);
            this.Login_Button.TabIndex = 2;
            this.Login_Button.Text = "Login";
            this.Login_Button.UseVisualStyleBackColor = true;
            this.Login_Button.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UserMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1013, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // UserMenu
            // 
            this.UserMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu2ToolStripMenuItem});
            this.UserMenu.Name = "UserMenu";
            this.UserMenu.Size = new System.Drawing.Size(70, 24);
            this.UserMenu.Text = "menu 1";
            // 
            // menu2ToolStripMenuItem
            // 
            this.menu2ToolStripMenuItem.Name = "menu2ToolStripMenuItem";
            this.menu2ToolStripMenuItem.Size = new System.Drawing.Size(133, 26);
            this.menu2ToolStripMenuItem.Text = "menu 2";
            // 
            // Logout_button
            // 
            this.Logout_button.Location = new System.Drawing.Point(809, 49);
            this.Logout_button.Name = "Logout_button";
            this.Logout_button.Size = new System.Drawing.Size(75, 35);
            this.Logout_button.TabIndex = 4;
            this.Logout_button.Text = "Log Out";
            this.Logout_button.UseVisualStyleBackColor = true;
            this.Logout_button.Click += new System.EventHandler(this.Logout_button_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Password);
            this.panel1.Controls.Add(this.Username);
            this.panel1.Controls.Add(this.userpass_input);
            this.panel1.Controls.Add(this.username_input);
            this.panel1.Controls.Add(this.Login_Button);
            this.panel1.Location = new System.Drawing.Point(12, 69);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(515, 250);
            this.panel1.TabIndex = 5;
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Location = new System.Drawing.Point(143, 98);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(69, 17);
            this.Password.TabIndex = 6;
            this.Password.Text = "Password";
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Location = new System.Drawing.Point(139, 48);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(73, 17);
            this.Username.TabIndex = 5;
            this.Username.Text = "Username";
            // 
            // userpass_input
            // 
            this.userpass_input.Location = new System.Drawing.Point(247, 95);
            this.userpass_input.Name = "userpass_input";
            this.userpass_input.Size = new System.Drawing.Size(198, 22);
            this.userpass_input.TabIndex = 4;
            // 
            // username_input
            // 
            this.username_input.Location = new System.Drawing.Point(247, 45);
            this.username_input.Name = "username_input";
            this.username_input.Size = new System.Drawing.Size(198, 22);
            this.username_input.TabIndex = 3;
            // 
            // Edit_user_info
            // 
            this.Edit_user_info.Location = new System.Drawing.Point(809, 504);
            this.Edit_user_info.Name = "Edit_user_info";
            this.Edit_user_info.Size = new System.Drawing.Size(180, 35);
            this.Edit_user_info.TabIndex = 6;
            this.Edit_user_info.Text = "Edit User Informtion";
            this.Edit_user_info.UseVisualStyleBackColor = true;
            // 
            // user_name
            // 
            this.user_name.AutoSize = true;
            this.user_name.Location = new System.Drawing.Point(898, 110);
            this.user_name.Name = "user_name";
            this.user_name.Size = new System.Drawing.Size(90, 17);
            this.user_name.TabIndex = 7;
            this.user_name.Text = "Please Login";
            // 
            // report_bug
            // 
            this.report_bug.Location = new System.Drawing.Point(632, 271);
            this.report_bug.Name = "report_bug";
            this.report_bug.Size = new System.Drawing.Size(116, 44);
            this.report_bug.TabIndex = 8;
            this.report_bug.Text = "Report a Bug";
            this.report_bug.UseVisualStyleBackColor = true;
            this.report_bug.Click += new System.EventHandler(this.report_bug_Click);
            // 
            // create_project
            // 
            this.create_project.Location = new System.Drawing.Point(632, 220);
            this.create_project.Name = "create_project";
            this.create_project.Size = new System.Drawing.Size(116, 44);
            this.create_project.TabIndex = 9;
            this.create_project.Text = "Create a Project";
            this.create_project.UseVisualStyleBackColor = true;
            this.create_project.Click += new System.EventHandler(this.create_project_Click);
            // 
            // my_projects
            // 
            this.my_projects.Location = new System.Drawing.Point(754, 219);
            this.my_projects.Name = "my_projects";
            this.my_projects.Size = new System.Drawing.Size(116, 44);
            this.my_projects.TabIndex = 10;
            this.my_projects.Text = "Projects";
            this.my_projects.UseVisualStyleBackColor = true;
            this.my_projects.Click += new System.EventHandler(this.my_projects_Click);
            // 
            // fix_bugs
            // 
            this.fix_bugs.Location = new System.Drawing.Point(754, 271);
            this.fix_bugs.Name = "fix_bugs";
            this.fix_bugs.Size = new System.Drawing.Size(116, 44);
            this.fix_bugs.TabIndex = 11;
            this.fix_bugs.Text = "Fix Bugs";
            this.fix_bugs.UseVisualStyleBackColor = true;
            this.fix_bugs.Click += new System.EventHandler(this.fix_bugs_Click);
            // 
            // my_questions
            // 
            this.my_questions.Location = new System.Drawing.Point(632, 321);
            this.my_questions.Name = "my_questions";
            this.my_questions.Size = new System.Drawing.Size(116, 44);
            this.my_questions.TabIndex = 12;
            this.my_questions.Text = "My Questions";
            this.my_questions.UseVisualStyleBackColor = true;
            this.my_questions.Click += new System.EventHandler(this.my_questions_Click);
            // 
            // my_answers
            // 
            this.my_answers.Location = new System.Drawing.Point(754, 321);
            this.my_answers.Name = "my_answers";
            this.my_answers.Size = new System.Drawing.Size(116, 44);
            this.my_answers.TabIndex = 13;
            this.my_answers.Text = "My Answers";
            this.my_answers.UseVisualStyleBackColor = true;
            this.my_answers.Click += new System.EventHandler(this.my_answers_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 551);
            this.Controls.Add(this.my_answers);
            this.Controls.Add(this.my_questions);
            this.Controls.Add(this.fix_bugs);
            this.Controls.Add(this.my_projects);
            this.Controls.Add(this.create_project);
            this.Controls.Add(this.report_bug);
            this.Controls.Add(this.user_name);
            this.Controls.Add(this.Edit_user_info);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Logout_button);
            this.Controls.Add(this.SignUp_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SignUp_Button;
        private System.Windows.Forms.Button Login_Button;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem UserMenu;
        private System.Windows.Forms.ToolStripMenuItem menu2ToolStripMenuItem;
        private System.Windows.Forms.Button Logout_button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox userpass_input;
        private System.Windows.Forms.TextBox username_input;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Button Edit_user_info;
        private System.Windows.Forms.Label user_name;
        private System.Windows.Forms.Button report_bug;
        private System.Windows.Forms.Button create_project;
        private System.Windows.Forms.Button my_projects;
        private System.Windows.Forms.Button fix_bugs;
        private System.Windows.Forms.Button my_questions;
        private System.Windows.Forms.Button my_answers;
    }
}

