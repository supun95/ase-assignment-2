﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class List : Form
    {
        /// <summary>
        /// membername passed through
        /// </summary>
        public string authenticated_user { get; set; }
        public String clicked_bug;

        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        /// <param name="membername"></param>
        public List(string membername)
        {           
            InitializeComponent();
            this.authenticated_user = membername;
            database_connection();
            buglist_ALL();
            bugs_assigned();
            severitytype();
        }

        /// <summary>
        /// setting up the sql connection
        /// </summary>
        SqlConnection mySqlConnection;

        /// <summary>
        /// database connection is set up to query to the database via the form
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// bug severity type filtration combobox facility 
        /// </summary>
        public void severitytype()
        {
            sev_type.Items.Add("Critical");
            sev_type.Items.Add("Medium");
            sev_type.Items.Add("Minor");
        }

        /// <summary>
        /// populating all the bugs required fixing 
        /// </summary>
        public void buglist_ALL()
        {
            mySqlConnection.Open();
            String selectcommand = "SELECT * FROM Bugs where bug_status != 'Bug is Fixed' AND bug_status != 'Pending Approval'";
            SqlCommand mySqlCommand = new SqlCommand(selectcommand, mySqlConnection);
            try
            {
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                bug_req_fix.Items.Clear();//clear exisiting items to prevent repated data

                while (mySqlDataReader.Read())
                {
                    bug_req_fix.Items.Add(mySqlDataReader["file_name"]);
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            mySqlConnection.Close();
        }

        /// <summary>
        /// populating bugs that are assigned to the user
        /// </summary>
        public void bugs_assigned()
        {
            mySqlConnection.Open();
            String selectcommand = "SELECT * FROM Bugs where bug_status != 'Bug is Fixed' AND bug_status != 'Pending Approval' AND assignuser = '" + this.authenticated_user + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(selectcommand, mySqlConnection);
            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                my_bugs.Items.Clear();

                while (mySqlDataReader2.Read())
                {
                    my_bugs.Items.Add(mySqlDataReader2["file_name"]);
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();

        }

        
        /// <summary>
        /// when a bug name is selected all the textboxes are populated with relevant information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_req_fix_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selectcommand = "SELECT * FROM Bugs WHERE file_name = '" + bug_req_fix.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(selectcommand, mySqlConnection);
            mySqlConnection.Open();
            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bug_name.Text = mySqlDataReader2["file_name"].ToString();
                    clicked_bug = mySqlDataReader2["file_name"].ToString();
                    member.Text = mySqlDataReader2["membername"].ToString();
                    severity.Text = mySqlDataReader2["severity"].ToString();
                    priority.Text = mySqlDataReader2["priority"].ToString();
                    code.Text = mySqlDataReader2["code_type"].ToString();
                    os.Text = mySqlDataReader2["os"].ToString();
                    date.Text = mySqlDataReader2["submit_date"].ToString(); 
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// when a bug name is fixed all the textboxes are populated with relevant information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_bugs_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selectcommand = "SELECT * FROM Bugs WHERE file_name = '" + my_bugs.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(selectcommand, mySqlConnection);
            mySqlConnection.Open();
            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bug_name.Text = mySqlDataReader2["file_name"].ToString();
                    clicked_bug = mySqlDataReader2["file_name"].ToString();
                    member.Text = mySqlDataReader2["membername"].ToString();
                    severity.Text = mySqlDataReader2["severity"].ToString();
                    priority.Text = mySqlDataReader2["priority"].ToString();
                    code.Text = mySqlDataReader2["code_type"].ToString();
                    os.Text = mySqlDataReader2["os"].ToString();
                    date.Text = mySqlDataReader2["submit_date"].ToString();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// severity type selection and bug identification using the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sev_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            String selectcommand = "SELECT * FROM Bugs WHERE severity = '" + sev_type.Text + "' AND bug_status != 'Bug is Fixed' AND bug_status != 'Pending Approval'";
            SqlCommand mySqlCommand4 = new SqlCommand(selectcommand, mySqlConnection);
            bug_filt_list.Items.Clear();

            try
            {
                SqlDataReader mySqlDataReader = mySqlCommand4.ExecuteReader();
                while (mySqlDataReader.Read())
                {
                    bug_filt_list.Items.Add(mySqlDataReader["file_name"]);
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// filtering bugnames form the bug filtration list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_filt_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selectcommand = "SELECT * FROM Bugs WHERE file_name = '" + bug_filt_list.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(selectcommand, mySqlConnection);
            mySqlConnection.Open();
            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bug_name.Text = mySqlDataReader2["file_name"].ToString();
                    clicked_bug = mySqlDataReader2["file_name"].ToString();
                    member.Text = mySqlDataReader2["membername"].ToString();
                    severity.Text = mySqlDataReader2["severity"].ToString();
                    priority.Text = mySqlDataReader2["priority"].ToString();
                    code.Text = mySqlDataReader2["code_type"].ToString();
                    os.Text = mySqlDataReader2["os"].ToString();
                    date.Text = mySqlDataReader2["submit_date"].ToString();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// validation of data that the user has selected. bugname text box cannot be empty
        /// </summary>
        /// <returns></returns>
        public bool data_validate()
        {
            bool returnvalue = true;

            if (
                string.IsNullOrEmpty(bug_name.Text))
            {
                MessageBox.Show("Error: Please select a bug from the listbox");
                returnvalue = false;
            }

            return (returnvalue);

        }

        /// <summary>
        /// when the fix bug is clicked bugname and the username will be passed into the fix bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fix_bug_Click(object sender, EventArgs e)
        {
            if (data_validate())
            {
                FixBug fixbugform = new FixBug(this.authenticated_user, clicked_bug);
                fixbugform.Show();
                this.Close();
            }
            
        }

        /// <summary>
        /// opens up the bug audit form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (data_validate())
            {
                BugAudit fixbugform = new BugAudit(clicked_bug, this.authenticated_user);
                fixbugform.Show();
            }
               
        }
    }
}
