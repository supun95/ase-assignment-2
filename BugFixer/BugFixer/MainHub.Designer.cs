﻿namespace BugFixer
{
    partial class MainHub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SignUp_Button = new System.Windows.Forms.Button();
            this.Login_Button = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.UserMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menu2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Logout_button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Password = new System.Windows.Forms.Label();
            this.Username = new System.Windows.Forms.Label();
            this.userpass_input = new System.Windows.Forms.TextBox();
            this.username_input = new System.Windows.Forms.TextBox();
            this.user_name = new System.Windows.Forms.Label();
            this.report_bug = new System.Windows.Forms.Button();
            this.create_project = new System.Windows.Forms.Button();
            this.my_projects = new System.Windows.Forms.Button();
            this.fix_bugs = new System.Windows.Forms.Button();
            this.my_questions = new System.Windows.Forms.Button();
            this.my_answers = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.questionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myQuestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myAnswersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(448, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Fixer";
            // 
            // SignUp_Button
            // 
            this.SignUp_Button.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.SignUp_Button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SignUp_Button.Location = new System.Drawing.Point(1014, 49);
            this.SignUp_Button.Name = "SignUp_Button";
            this.SignUp_Button.Size = new System.Drawing.Size(99, 35);
            this.SignUp_Button.TabIndex = 1;
            this.SignUp_Button.Text = "Sign Up";
            this.SignUp_Button.UseVisualStyleBackColor = false;
            this.SignUp_Button.Click += new System.EventHandler(this.SignUpButton_Click);
            // 
            // Login_Button
            // 
            this.Login_Button.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Login_Button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Login_Button.Location = new System.Drawing.Point(536, 238);
            this.Login_Button.Name = "Login_Button";
            this.Login_Button.Size = new System.Drawing.Size(84, 35);
            this.Login_Button.TabIndex = 2;
            this.Login_Button.Text = "Login";
            this.Login_Button.UseVisualStyleBackColor = false;
            this.Login_Button.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UserMenu,
            this.bugsToolStripMenuItem,
            this.questionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1140, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // UserMenu
            // 
            this.UserMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu2ToolStripMenuItem,
            this.projectListToolStripMenuItem});
            this.UserMenu.Name = "UserMenu";
            this.UserMenu.Size = new System.Drawing.Size(73, 24);
            this.UserMenu.Text = "Projects";
            // 
            // menu2ToolStripMenuItem
            // 
            this.menu2ToolStripMenuItem.Name = "menu2ToolStripMenuItem";
            this.menu2ToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.menu2ToolStripMenuItem.Text = "New Project";
            this.menu2ToolStripMenuItem.Click += new System.EventHandler(this.menu2ToolStripMenuItem_Click);
            // 
            // Logout_button
            // 
            this.Logout_button.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Logout_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Logout_button.Location = new System.Drawing.Point(910, 49);
            this.Logout_button.Name = "Logout_button";
            this.Logout_button.Size = new System.Drawing.Size(84, 35);
            this.Logout_button.TabIndex = 4;
            this.Logout_button.Text = "Log Out";
            this.Logout_button.UseVisualStyleBackColor = false;
            this.Logout_button.Click += new System.EventHandler(this.Logout_button_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Password);
            this.panel1.Controls.Add(this.Username);
            this.panel1.Controls.Add(this.userpass_input);
            this.panel1.Controls.Add(this.username_input);
            this.panel1.Controls.Add(this.Login_Button);
            this.panel1.Location = new System.Drawing.Point(12, 142);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1116, 386);
            this.panel1.TabIndex = 5;
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Location = new System.Drawing.Point(387, 181);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(77, 17);
            this.Password.TabIndex = 6;
            this.Password.Text = "Password";
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Location = new System.Drawing.Point(382, 131);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(81, 17);
            this.Username.TabIndex = 5;
            this.Username.Text = "Username";
            // 
            // userpass_input
            // 
            this.userpass_input.Location = new System.Drawing.Point(504, 178);
            this.userpass_input.Name = "userpass_input";
            this.userpass_input.Size = new System.Drawing.Size(222, 22);
            this.userpass_input.TabIndex = 4;
            // 
            // username_input
            // 
            this.username_input.Location = new System.Drawing.Point(504, 128);
            this.username_input.Name = "username_input";
            this.username_input.Size = new System.Drawing.Size(222, 22);
            this.username_input.TabIndex = 3;
            // 
            // user_name
            // 
            this.user_name.AutoSize = true;
            this.user_name.Location = new System.Drawing.Point(12, 39);
            this.user_name.Name = "user_name";
            this.user_name.Size = new System.Drawing.Size(102, 17);
            this.user_name.TabIndex = 7;
            this.user_name.Text = "Please Login";
            // 
            // report_bug
            // 
            this.report_bug.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.report_bug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.report_bug.Location = new System.Drawing.Point(371, 31);
            this.report_bug.Name = "report_bug";
            this.report_bug.Size = new System.Drawing.Size(130, 44);
            this.report_bug.TabIndex = 8;
            this.report_bug.Text = "Report a Bug";
            this.report_bug.UseVisualStyleBackColor = false;
            this.report_bug.Click += new System.EventHandler(this.report_bug_Click);
            // 
            // create_project
            // 
            this.create_project.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.create_project.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.create_project.Location = new System.Drawing.Point(18, 31);
            this.create_project.Name = "create_project";
            this.create_project.Size = new System.Drawing.Size(130, 44);
            this.create_project.TabIndex = 9;
            this.create_project.Text = "Create a Project";
            this.create_project.UseVisualStyleBackColor = false;
            this.create_project.Click += new System.EventHandler(this.create_project_Click);
            // 
            // my_projects
            // 
            this.my_projects.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.my_projects.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.my_projects.Location = new System.Drawing.Point(195, 31);
            this.my_projects.Name = "my_projects";
            this.my_projects.Size = new System.Drawing.Size(130, 44);
            this.my_projects.TabIndex = 10;
            this.my_projects.Text = "Projects";
            this.my_projects.UseVisualStyleBackColor = false;
            this.my_projects.Click += new System.EventHandler(this.my_projects_Click);
            // 
            // fix_bugs
            // 
            this.fix_bugs.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.fix_bugs.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fix_bugs.Location = new System.Drawing.Point(546, 31);
            this.fix_bugs.Name = "fix_bugs";
            this.fix_bugs.Size = new System.Drawing.Size(130, 44);
            this.fix_bugs.TabIndex = 11;
            this.fix_bugs.Text = "Fix Bugs";
            this.fix_bugs.UseVisualStyleBackColor = false;
            this.fix_bugs.Click += new System.EventHandler(this.fix_bugs_Click);
            // 
            // my_questions
            // 
            this.my_questions.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.my_questions.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.my_questions.Location = new System.Drawing.Point(721, 31);
            this.my_questions.Name = "my_questions";
            this.my_questions.Size = new System.Drawing.Size(130, 44);
            this.my_questions.TabIndex = 12;
            this.my_questions.Text = "My Questions";
            this.my_questions.UseVisualStyleBackColor = false;
            this.my_questions.Click += new System.EventHandler(this.my_questions_Click);
            // 
            // my_answers
            // 
            this.my_answers.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.my_answers.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.my_answers.Location = new System.Drawing.Point(899, 31);
            this.my_answers.Name = "my_answers";
            this.my_answers.Size = new System.Drawing.Size(130, 44);
            this.my_answers.TabIndex = 13;
            this.my_answers.Text = "My Answers";
            this.my_answers.UseVisualStyleBackColor = false;
            this.my_answers.Click += new System.EventHandler(this.my_answers_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.create_project);
            this.panel2.Controls.Add(this.my_answers);
            this.panel2.Controls.Add(this.my_projects);
            this.panel2.Controls.Add(this.my_questions);
            this.panel2.Controls.Add(this.report_bug);
            this.panel2.Controls.Add(this.fix_bugs);
            this.panel2.Location = new System.Drawing.Point(56, 340);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1056, 104);
            this.panel2.TabIndex = 14;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BugFixer.Properties.Resources.bugimage1;
            this.pictureBox1.Location = new System.Drawing.Point(214, 192);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(780, 129);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(505, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(337, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "Welcome to Bug Fixer Application";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(505, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(393, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Choose a following option to proceed with your query";
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportBugToolStripMenuItem,
            this.bugListToolStripMenuItem});
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.bugsToolStripMenuItem.Text = "Bugs";
            // 
            // questionsToolStripMenuItem
            // 
            this.questionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myQuestionToolStripMenuItem,
            this.myAnswersToolStripMenuItem});
            this.questionsToolStripMenuItem.Name = "questionsToolStripMenuItem";
            this.questionsToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.questionsToolStripMenuItem.Text = "Questions";
            // 
            // projectListToolStripMenuItem
            // 
            this.projectListToolStripMenuItem.Name = "projectListToolStripMenuItem";
            this.projectListToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.projectListToolStripMenuItem.Text = "Project List";
            this.projectListToolStripMenuItem.Click += new System.EventHandler(this.projectListToolStripMenuItem_Click);
            // 
            // reportBugToolStripMenuItem
            // 
            this.reportBugToolStripMenuItem.Name = "reportBugToolStripMenuItem";
            this.reportBugToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.reportBugToolStripMenuItem.Text = "Report a Bug";
            this.reportBugToolStripMenuItem.Click += new System.EventHandler(this.reportBugToolStripMenuItem_Click);
            // 
            // bugListToolStripMenuItem
            // 
            this.bugListToolStripMenuItem.Name = "bugListToolStripMenuItem";
            this.bugListToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.bugListToolStripMenuItem.Text = "Bug List";
            this.bugListToolStripMenuItem.Click += new System.EventHandler(this.bugListToolStripMenuItem_Click);
            // 
            // myQuestionToolStripMenuItem
            // 
            this.myQuestionToolStripMenuItem.Name = "myQuestionToolStripMenuItem";
            this.myQuestionToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.myQuestionToolStripMenuItem.Text = "My Question";
            this.myQuestionToolStripMenuItem.Click += new System.EventHandler(this.myQuestionToolStripMenuItem_Click);
            // 
            // myAnswersToolStripMenuItem
            // 
            this.myAnswersToolStripMenuItem.Name = "myAnswersToolStripMenuItem";
            this.myAnswersToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.myAnswersToolStripMenuItem.Text = "My Answers";
            this.myAnswersToolStripMenuItem.Click += new System.EventHandler(this.myAnswersToolStripMenuItem_Click);
            // 
            // MainHub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1140, 551);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.user_name);
            this.Controls.Add(this.Logout_button);
            this.Controls.Add(this.SignUp_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainHub";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SignUp_Button;
        private System.Windows.Forms.Button Login_Button;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem UserMenu;
        private System.Windows.Forms.ToolStripMenuItem menu2ToolStripMenuItem;
        private System.Windows.Forms.Button Logout_button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox userpass_input;
        private System.Windows.Forms.TextBox username_input;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Label user_name;
        private System.Windows.Forms.Button report_bug;
        private System.Windows.Forms.Button create_project;
        private System.Windows.Forms.Button my_projects;
        private System.Windows.Forms.Button fix_bugs;
        private System.Windows.Forms.Button my_questions;
        private System.Windows.Forms.Button my_answers;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem projectListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportBugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bugListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem questionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myQuestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myAnswersToolStripMenuItem;
    }
}

