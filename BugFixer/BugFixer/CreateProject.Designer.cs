﻿namespace BugFixer
{
    partial class CreateProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.create_project = new System.Windows.Forms.Button();
            this.p_name = new System.Windows.Forms.TextBox();
            this.p_desc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(116, -111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 40;
            this.label7.Text = "My Projects";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 39;
            this.label6.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 17);
            this.label4.TabIndex = 37;
            this.label4.Text = "Project Name";
            // 
            // create_project
            // 
            this.create_project.Location = new System.Drawing.Point(322, 374);
            this.create_project.Name = "create_project";
            this.create_project.Size = new System.Drawing.Size(147, 41);
            this.create_project.TabIndex = 36;
            this.create_project.Text = "Create Project";
            this.create_project.UseVisualStyleBackColor = true;
            // 
            // p_name
            // 
            this.p_name.Location = new System.Drawing.Point(43, 90);
            this.p_name.Name = "p_name";
            this.p_name.Size = new System.Drawing.Size(251, 22);
            this.p_name.TabIndex = 34;
            // 
            // p_desc
            // 
            this.p_desc.Location = new System.Drawing.Point(43, 135);
            this.p_desc.Multiline = true;
            this.p_desc.Name = "p_desc";
            this.p_desc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.p_desc.Size = new System.Drawing.Size(706, 210);
            this.p_desc.TabIndex = 33;
            this.p_desc.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(357, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 41;
            this.label1.Text = "New Project";
            // 
            // CreateProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 456);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.create_project);
            this.Controls.Add(this.p_name);
            this.Controls.Add(this.p_desc);
            this.Name = "CreateProject";
            this.Text = "CreateProject";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button create_project;
        private System.Windows.Forms.TextBox p_name;
        private System.Windows.Forms.TextBox p_desc;
        private System.Windows.Forms.Label label1;
    }
}