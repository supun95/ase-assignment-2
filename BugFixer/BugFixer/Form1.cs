﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            menuStrip1.Visible = false;
            Logout_button.Visible = false;
            userpass_input.PasswordChar = '*'; 
        }

        SqlConnection mySqlConnection;
        public String authenticated_member;
        public string MyProperty { get; set; }

        public static string Md5PasswordHash(string text)
        {
            MD5 md5_password_hash = new MD5CryptoServiceProvider();

            md5_password_hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5_password_hash.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + appPath + ";Integrated Security=True;Connect Timeout=30");
            mySqlConnection.Open();

            if (check_input_data())
            {
                string hashed_password = Md5PasswordHash(userpass_input.Text);
                String exisitng_user_identification = "SELECT COUNT(*) FROM users WHERE user_name = '" + username_input.Text + "' AND user_password = '" + hashed_password + "'";
                SqlCommand cmd_check_user = new SqlCommand(exisitng_user_identification, mySqlConnection);
                int user_identification = (int)cmd_check_user.ExecuteScalar();

                if (user_identification > 0)
                {
                    panel1.Visible = false;
                    menuStrip1.Visible = true;
                    SignUp_Button.Visible = false;
                    authenticated_member = username_input.Text;
                    user_name.Text = authenticated_member;
                    Logout_button.Visible = true;
                    Edit_user_info.Visible = true;
                }
                else
                {
                    MessageBox.Show("Error: User does not exist");
                }
            }
        }

        public bool check_input_data()
        {
            bool returnvalue = true;

            if (
                string.IsNullOrEmpty(username_input.Text)
                || string.IsNullOrEmpty(userpass_input.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                returnvalue = false;
            }

            return (returnvalue);

        }


        private void SignUpButton_Click(object sender, EventArgs e)
        {
            SignUp signupform = new SignUp();
            signupform.Show();
        }

        private void Logout_button_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            user_name.Text = "Please login";
            username_input.Text = userpass_input.Text = "";
            Edit_user_info.Visible = false;
            menuStrip1.Visible = false;
            Logout_button.Visible = false;
            SignUp_Button.Visible = true;
        }

        /// <summary>
        /// opens the report bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void report_bug_Click(object sender, EventArgs e)
        {
            ReportBug reportbugForm = new ReportBug(authenticated_member);
            reportbugForm.Show();
        }

        /// <summary>
        /// opens the create project form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void create_project_Click(object sender, EventArgs e)
        {
            NewProject createprojectForm = new NewProject(authenticated_member);
            createprojectForm.Show();
        }

        /// <summary>
        /// opens the projects form. All the projects that are created by the member can be seen on this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_projects_Click(object sender, EventArgs e)
        {
            MyProjects projectsForm = new MyProjects(authenticated_member);
            projectsForm.Show();
        }

        /// <summary>
        /// opens the list of bugs that requires fixing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fix_bugs_Click(object sender, EventArgs e)
        {
            List buglist = new List(authenticated_member);
            buglist.Show();
        }

        /// <summary>
        /// opens my questions form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_questions_Click(object sender, EventArgs e)
        {
            MyQuestions questionlist = new MyQuestions(authenticated_member);
            questionlist.Show();
        }

        /// <summary>
        /// opens bug solutions list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_answers_Click(object sender, EventArgs e)
        {
            BugSolList bugsollist = new BugSolList(authenticated_member);
            bugsollist.Show();
        }
    }
}
