﻿namespace BugFixer
{
    partial class FixBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bug_name = new System.Windows.Forms.TextBox();
            this.member = new System.Windows.Forms.TextBox();
            this.ctype = new System.Windows.Forms.TextBox();
            this.os = new System.Windows.Forms.TextBox();
            this.date_posted = new System.Windows.Forms.TextBox();
            this.priority = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bug_desc = new System.Windows.Forms.TextBox();
            this.sol_code = new System.Windows.Forms.TextBox();
            this.sol_desc = new System.Windows.Forms.TextBox();
            this.download_code = new System.Windows.Forms.Button();
            this.upload_code = new System.Windows.Forms.Button();
            this.submit_sol = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.email_check = new System.Windows.Forms.CheckBox();
            this.bug_code = new System.Windows.Forms.WebBrowser();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.col_code = new System.Windows.Forms.WebBrowser();
            this.label6 = new System.Windows.Forms.Label();
            this.line_no = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(644, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(378, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fix the Bug";
            // 
            // bug_name
            // 
            this.bug_name.Location = new System.Drawing.Point(168, 162);
            this.bug_name.Name = "bug_name";
            this.bug_name.Size = new System.Drawing.Size(184, 22);
            this.bug_name.TabIndex = 1;
            // 
            // member
            // 
            this.member.Location = new System.Drawing.Point(168, 206);
            this.member.Name = "member";
            this.member.Size = new System.Drawing.Size(184, 22);
            this.member.TabIndex = 2;
            // 
            // ctype
            // 
            this.ctype.Location = new System.Drawing.Point(168, 284);
            this.ctype.Name = "ctype";
            this.ctype.Size = new System.Drawing.Size(184, 22);
            this.ctype.TabIndex = 3;
            // 
            // os
            // 
            this.os.Location = new System.Drawing.Point(168, 324);
            this.os.Name = "os";
            this.os.Size = new System.Drawing.Size(184, 22);
            this.os.TabIndex = 4;
            // 
            // date_posted
            // 
            this.date_posted.Location = new System.Drawing.Point(168, 366);
            this.date_posted.Name = "date_posted";
            this.date_posted.Size = new System.Drawing.Size(184, 22);
            this.date_posted.TabIndex = 6;
            // 
            // priority
            // 
            this.priority.Location = new System.Drawing.Point(168, 409);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(184, 22);
            this.priority.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Bug Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Member";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 287);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Code Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 329);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Operating System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Date Posted";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 409);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Priority";
            // 
            // bug_desc
            // 
            this.bug_desc.Location = new System.Drawing.Point(410, 490);
            this.bug_desc.Multiline = true;
            this.bug_desc.Name = "bug_desc";
            this.bug_desc.Size = new System.Drawing.Size(519, 69);
            this.bug_desc.TabIndex = 17;
            // 
            // sol_code
            // 
            this.sol_code.Location = new System.Drawing.Point(951, 97);
            this.sol_code.Multiline = true;
            this.sol_code.Name = "sol_code";
            this.sol_code.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sol_code.Size = new System.Drawing.Size(519, 334);
            this.sol_code.TabIndex = 18;
            this.sol_code.WordWrap = false;
            // 
            // sol_desc
            // 
            this.sol_desc.Location = new System.Drawing.Point(951, 490);
            this.sol_desc.Multiline = true;
            this.sol_desc.Name = "sol_desc";
            this.sol_desc.Size = new System.Drawing.Size(519, 69);
            this.sol_desc.TabIndex = 20;
            // 
            // download_code
            // 
            this.download_code.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.download_code.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.download_code.Location = new System.Drawing.Point(410, 442);
            this.download_code.Name = "download_code";
            this.download_code.Size = new System.Drawing.Size(154, 28);
            this.download_code.TabIndex = 21;
            this.download_code.Text = "Download Code";
            this.download_code.UseVisualStyleBackColor = false;
            this.download_code.Click += new System.EventHandler(this.download_code_Click);
            // 
            // upload_code
            // 
            this.upload_code.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.upload_code.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.upload_code.Location = new System.Drawing.Point(951, 443);
            this.upload_code.Name = "upload_code";
            this.upload_code.Size = new System.Drawing.Size(153, 27);
            this.upload_code.TabIndex = 22;
            this.upload_code.Text = "Upload Code";
            this.upload_code.UseVisualStyleBackColor = false;
            this.upload_code.Click += new System.EventHandler(this.upload_code_Click);
            // 
            // submit_sol
            // 
            this.submit_sol.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.submit_sol.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.submit_sol.Location = new System.Drawing.Point(678, 565);
            this.submit_sol.Name = "submit_sol";
            this.submit_sol.Size = new System.Drawing.Size(195, 34);
            this.submit_sol.TabIndex = 23;
            this.submit_sol.Text = "Submit Solution";
            this.submit_sol.UseVisualStyleBackColor = false;
            this.submit_sol.Click += new System.EventHandler(this.submit_sol_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(621, 470);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "Bug Description";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1149, 470);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "Solution Description";
            // 
            // email_check
            // 
            this.email_check.AutoSize = true;
            this.email_check.Location = new System.Drawing.Point(891, 573);
            this.email_check.Name = "email_check";
            this.email_check.Size = new System.Drawing.Size(108, 21);
            this.email_check.TabIndex = 26;
            this.email_check.Text = "Email Alert";
            this.email_check.UseVisualStyleBackColor = true;
            // 
            // bug_code
            // 
            this.bug_code.Location = new System.Drawing.Point(410, 96);
            this.bug_code.MinimumSize = new System.Drawing.Size(22, 20);
            this.bug_code.Name = "bug_code";
            this.bug_code.Size = new System.Drawing.Size(520, 334);
            this.bug_code.TabIndex = 19;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(410, 96);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(519, 334);
            this.textBox1.TabIndex = 27;
            this.textBox1.WordWrap = false;
            // 
            // col_code
            // 
            this.col_code.Location = new System.Drawing.Point(951, 97);
            this.col_code.MinimumSize = new System.Drawing.Size(22, 20);
            this.col_code.Name = "col_code";
            this.col_code.Size = new System.Drawing.Size(520, 334);
            this.col_code.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Line Number";
            // 
            // line_no
            // 
            this.line_no.Location = new System.Drawing.Point(168, 243);
            this.line_no.Name = "line_no";
            this.line_no.Size = new System.Drawing.Size(184, 22);
            this.line_no.TabIndex = 29;
            // 
            // FixBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1502, 611);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.line_no);
            this.Controls.Add(this.col_code);
            this.Controls.Add(this.email_check);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.submit_sol);
            this.Controls.Add(this.upload_code);
            this.Controls.Add(this.download_code);
            this.Controls.Add(this.sol_desc);
            this.Controls.Add(this.bug_code);
            this.Controls.Add(this.sol_code);
            this.Controls.Add(this.bug_desc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.date_posted);
            this.Controls.Add(this.os);
            this.Controls.Add(this.ctype);
            this.Controls.Add(this.member);
            this.Controls.Add(this.bug_name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "FixBug";
            this.Text = "FixBug";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bug_name;
        private System.Windows.Forms.TextBox member;
        private System.Windows.Forms.TextBox ctype;
        private System.Windows.Forms.TextBox os;
        private System.Windows.Forms.TextBox date_posted;
        private System.Windows.Forms.TextBox priority;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox bug_desc;
        private System.Windows.Forms.TextBox sol_code;
        private System.Windows.Forms.TextBox sol_desc;
        private System.Windows.Forms.Button download_code;
        private System.Windows.Forms.Button upload_code;
        private System.Windows.Forms.Button submit_sol;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox email_check;
        private System.Windows.Forms.WebBrowser bug_code;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.WebBrowser col_code;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox line_no;
    }
}