﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class MyProjects : Form
    {
        //to obtain user details
        public string authenticated_user { get; set; }

        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        /// <param name="membername"></param>
        public MyProjects(string membername)
        {
            InitializeComponent();
            this.authenticated_user = membername;
            database_connection();
            infoDisplay();
        }

        /// <summary>
        /// initiates the database connection
        /// </summary>
        SqlConnection mySqlConnection;
        public String clicked_bug;

        /// <summary>
        /// database connection is set up to query to the database via the form
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// displays the list of projects, when the user opens the form
        /// </summary>
        public void infoDisplay()
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM Projects";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);
            try
            {

                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                p_list.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    p_list.Items.Add(mySqlDataReader["project_name"]);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// when the user clicks on the project name, its details will be displayed on textboxes. Moreover, bugs accociated with each project will be displayed in the other listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void p_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            mySqlConnection.Open();

            String command1 = "SELECT * FROM Projects WHERE project_name = '" + p_list.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(command1, mySqlConnection);

            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    p_name.Text = mySqlDataReader2["project_name"].ToString();
                    p_date.Text = mySqlDataReader2["date"].ToString();
                    p_desc.Text = mySqlDataReader2["project_description"].ToString();
                    use_create.Text = mySqlDataReader2["user_name"].ToString();
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();



            mySqlConnection.Open();
            //obtain  bugs for the selected project
            String command2 = "SELECT * FROM Bugs WHERE project_name = '" + p_list.Text + "'";
            SqlCommand mySqlCommand3 = new SqlCommand(command2, mySqlConnection);
            bug_list.Items.Clear();//clear exisiting items

            try
            {
                SqlDataReader mySqlDataReader3 = mySqlCommand3.ExecuteReader();
                while (mySqlDataReader3.Read())
                {
                    bug_list.Items.Add(mySqlDataReader3["file_name"]);
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// validates user inputs
        /// </summary>
        /// <returns></returns>
        public bool Validate_data()
        {
            bool returnvalue = true;

            if (
                 string.IsNullOrEmpty(selec_bug.Text))
            {
                MessageBox.Show("Error: Please choose a bug from the bug list");
                returnvalue = false;
            }

            return (returnvalue);
        }

        /// <summary>
        /// transferes the user selected value into the texbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            selec_bug.Text = bug_list.Text;
        }

        private void bug_hist_Click(object sender, EventArgs e)
        {
            if (Validate_data())
            {
                BugAudit newForm = new BugAudit(selec_bug.Text, this.authenticated_user);
                newForm.Show();
            }
        }
    }
}
