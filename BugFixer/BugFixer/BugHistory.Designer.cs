﻿namespace BugFixer
{
    partial class BugHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bug_code = new System.Windows.Forms.WebBrowser();
            this.bug_code_2 = new System.Windows.Forms.WebBrowser();
            this.commit_list = new System.Windows.Forms.ListBox();
            this.download_code = new System.Windows.Forms.Button();
            this.download_code2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.b_desc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.date_posted = new System.Windows.Forms.TextBox();
            this.c_type = new System.Windows.Forms.TextBox();
            this.op = new System.Windows.Forms.TextBox();
            this.fixer = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.TextBox();
            this.bug_name = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bug_code
            // 
            this.bug_code.Location = new System.Drawing.Point(396, 112);
            this.bug_code.MinimumSize = new System.Drawing.Size(20, 20);
            this.bug_code.Name = "bug_code";
            this.bug_code.Size = new System.Drawing.Size(462, 334);
            this.bug_code.TabIndex = 20;
            // 
            // bug_code_2
            // 
            this.bug_code_2.Location = new System.Drawing.Point(864, 112);
            this.bug_code_2.MinimumSize = new System.Drawing.Size(20, 20);
            this.bug_code_2.Name = "bug_code_2";
            this.bug_code_2.Size = new System.Drawing.Size(462, 334);
            this.bug_code_2.TabIndex = 21;
            // 
            // commit_list
            // 
            this.commit_list.FormattingEnabled = true;
            this.commit_list.HorizontalScrollbar = true;
            this.commit_list.ItemHeight = 16;
            this.commit_list.Location = new System.Drawing.Point(12, 112);
            this.commit_list.Name = "commit_list";
            this.commit_list.ScrollAlwaysVisible = true;
            this.commit_list.Size = new System.Drawing.Size(148, 468);
            this.commit_list.TabIndex = 28;
            // 
            // download_code
            // 
            this.download_code.Location = new System.Drawing.Point(396, 452);
            this.download_code.Name = "download_code";
            this.download_code.Size = new System.Drawing.Size(137, 28);
            this.download_code.TabIndex = 29;
            this.download_code.Text = "Download Code";
            this.download_code.UseVisualStyleBackColor = true;
            // 
            // download_code2
            // 
            this.download_code2.Location = new System.Drawing.Point(864, 452);
            this.download_code2.Name = "download_code2";
            this.download_code2.Size = new System.Drawing.Size(137, 28);
            this.download_code2.TabIndex = 30;
            this.download_code2.Text = "Download Code";
            this.download_code2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(393, 483);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "Description";
            // 
            // b_desc
            // 
            this.b_desc.Location = new System.Drawing.Point(396, 503);
            this.b_desc.Multiline = true;
            this.b_desc.Name = "b_desc";
            this.b_desc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.b_desc.Size = new System.Drawing.Size(930, 76);
            this.b_desc.TabIndex = 40;
            this.b_desc.WordWrap = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(198, 527);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 53;
            this.label7.Text = "Date Posted";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(194, 452);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 52;
            this.label1.Text = "Code Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(198, 371);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 17);
            this.label5.TabIndex = 51;
            this.label5.Text = "Original Poster";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(198, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 50;
            this.label4.Text = "Fixer";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(195, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 49;
            this.label3.Text = "Bug Status";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 48;
            this.label2.Text = "Bug Name";
            // 
            // date_posted
            // 
            this.date_posted.Location = new System.Drawing.Point(198, 557);
            this.date_posted.Name = "date_posted";
            this.date_posted.Size = new System.Drawing.Size(164, 22);
            this.date_posted.TabIndex = 47;
            // 
            // c_type
            // 
            this.c_type.Location = new System.Drawing.Point(198, 480);
            this.c_type.Name = "c_type";
            this.c_type.Size = new System.Drawing.Size(164, 22);
            this.c_type.TabIndex = 46;
            // 
            // op
            // 
            this.op.Location = new System.Drawing.Point(198, 404);
            this.op.Name = "op";
            this.op.Size = new System.Drawing.Size(164, 22);
            this.op.TabIndex = 45;
            // 
            // fixer
            // 
            this.fixer.Location = new System.Drawing.Point(198, 314);
            this.fixer.Name = "fixer";
            this.fixer.Size = new System.Drawing.Size(164, 22);
            this.fixer.TabIndex = 44;
            // 
            // status
            // 
            this.status.Location = new System.Drawing.Point(198, 227);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(164, 22);
            this.status.TabIndex = 43;
            // 
            // bug_name
            // 
            this.bug_name.Location = new System.Drawing.Point(198, 140);
            this.bug_name.Name = "bug_name";
            this.bug_name.Size = new System.Drawing.Size(164, 22);
            this.bug_name.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(644, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 17);
            this.label8.TabIndex = 54;
            this.label8.Text = "Bug History";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 55;
            this.label9.Text = "Commits";
            // 
            // BugHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1338, 591);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.date_posted);
            this.Controls.Add(this.c_type);
            this.Controls.Add(this.op);
            this.Controls.Add(this.fixer);
            this.Controls.Add(this.status);
            this.Controls.Add(this.bug_name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.b_desc);
            this.Controls.Add(this.download_code2);
            this.Controls.Add(this.download_code);
            this.Controls.Add(this.commit_list);
            this.Controls.Add(this.bug_code_2);
            this.Controls.Add(this.bug_code);
            this.Name = "BugHistory";
            this.Text = "BugHistory";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser bug_code;
        private System.Windows.Forms.WebBrowser bug_code_2;
        private System.Windows.Forms.ListBox commit_list;
        private System.Windows.Forms.Button download_code;
        private System.Windows.Forms.Button download_code2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox b_desc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox date_posted;
        private System.Windows.Forms.TextBox c_type;
        private System.Windows.Forms.TextBox op;
        private System.Windows.Forms.TextBox fixer;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.TextBox bug_name;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}