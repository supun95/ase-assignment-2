﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColorCode;

namespace BugFixer
{
    public partial class BugAudit : Form
    {
        //for passing bugname and username
        public string authenticateduser { get; set; }
        public string passedbug { get; set; }

        /// <summary>
        /// Relevant methods are initialised when the form loads 
        /// </summary>
        /// <param name="passed_bug"></param>
        /// <param name="membername"></param>
        public BugAudit(string passed_bug, string membername)
        {
            InitializeComponent();
            database_connection();
            this.authenticateduser = membername;
            this.passedbug = passed_bug;
            commitlist();
        }

        /// <summary>
        /// calling sql connection
        /// </summary>
        SqlConnection mySqlConnection;

        /// <summary>
        /// Setting up the data connection
        /// </summary>
        public void database_connection()
        {
            //string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// add values to the list box by quering the database values
        /// </summary>
        public void commitlist()
        {
            mySqlConnection.Open();
            String command = "SELECT bugaudit_Id FROM BugAudit WHERE file_name = '" + this.passedbug + "' ORDER BY bugaudit_Id DESC";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);
            try
            {
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                commit_list.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    commit_list.Items.Add(mySqlDataReader["bugaudit_Id"]);//displays commitlist
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// when a commit from the listbox is selected, all the details related to that commit will be populated on to textboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commit_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM BugAudit WHERE bugaudit_Id = '" + commit_list.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(command, mySqlConnection);

            try
            {//values are put in the textboxes
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bug_name.Text = mySqlDataReader2["file_name"].ToString();
                    c_type.Text = mySqlDataReader2["code_type"].ToString();
                    op.Text = mySqlDataReader2["membername"].ToString();
                    status.Text = mySqlDataReader2["bug_status"].ToString();
                    date_posted.Text = mySqlDataReader2["date"].ToString();
                    fixer.Text = mySqlDataReader2["fixers_name"].ToString();
                    textBox1.Text = mySqlDataReader2["initial_code"].ToString();
                    string initCode = mySqlDataReader2["initial_code"].ToString();
                    string colorizedinitCode = new CodeColorizer().Colorize(initCode, Languages.CSharp);
                    bug_code.DocumentText = colorizedinitCode;
                    textBox2.Text = mySqlDataReader2["final_code"].ToString();
                    string solCode = mySqlDataReader2["final_code"].ToString();
                    string colorizedsolCode = new CodeColorizer().Colorize(solCode, Languages.CSharp);
                    bug_code_2.DocumentText = colorizedsolCode;
                    b_desc.Text = mySqlDataReader2["description"].ToString();
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// If the user wants to download the sourcecode savefiledialog will be called and the user will be able to save the source file in a source of their liking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void download_code_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox1.Text);
            }
        }

        /// <summary>
        /// If the user wants to download the sourcecode savefiledialog will be called and the user will be able to save the source file in a source of their liking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void download_code2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";//allows to be saved as a txt file

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox2.Text);
            }
        }
    }
}
