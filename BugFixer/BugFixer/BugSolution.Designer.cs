﻿namespace BugFixer
{
    partial class BugSolution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Proj = new System.Windows.Forms.TextBox();
            this.priority = new System.Windows.Forms.TextBox();
            this.date_posted = new System.Windows.Forms.TextBox();
            this.op = new System.Windows.Forms.TextBox();
            this.os = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.TextBox();
            this.member = new System.Windows.Forms.TextBox();
            this.bug_name = new System.Windows.Forms.TextBox();
            this.bug_code_col = new System.Windows.Forms.WebBrowser();
            this.mod_code = new System.Windows.Forms.TextBox();
            this.download_code = new System.Windows.Forms.Button();
            this.mod_desc = new System.Windows.Forms.TextBox();
            this.reason = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.ComboBox();
            this.submit_query = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bugcode = new System.Windows.Forms.TextBox();
            this.mod_code_col = new System.Windows.Forms.WebBrowser();
            this.bug_audit = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 396);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 17);
            this.label9.TabIndex = 32;
            this.label9.Text = "Project";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 353);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Priority";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 30;
            this.label7.Text = "Date Posted";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 17);
            this.label6.TabIndex = 29;
            this.label6.Text = "Original Poster";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 17);
            this.label5.TabIndex = 28;
            this.label5.Text = "Operating System";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 27;
            this.label4.Text = "Code Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "Fixer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Bug Name";
            // 
            // Proj
            // 
            this.Proj.Location = new System.Drawing.Point(180, 391);
            this.Proj.Name = "Proj";
            this.Proj.Size = new System.Drawing.Size(184, 22);
            this.Proj.TabIndex = 24;
            // 
            // priority
            // 
            this.priority.Location = new System.Drawing.Point(180, 348);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(184, 22);
            this.priority.TabIndex = 23;
            // 
            // date_posted
            // 
            this.date_posted.Location = new System.Drawing.Point(180, 305);
            this.date_posted.Name = "date_posted";
            this.date_posted.Size = new System.Drawing.Size(184, 22);
            this.date_posted.TabIndex = 22;
            // 
            // op
            // 
            this.op.Location = new System.Drawing.Point(180, 168);
            this.op.Name = "op";
            this.op.Size = new System.Drawing.Size(184, 22);
            this.op.TabIndex = 21;
            // 
            // os
            // 
            this.os.Location = new System.Drawing.Point(180, 262);
            this.os.Name = "os";
            this.os.Size = new System.Drawing.Size(184, 22);
            this.os.TabIndex = 20;
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(180, 217);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(184, 22);
            this.code.TabIndex = 19;
            // 
            // member
            // 
            this.member.Location = new System.Drawing.Point(180, 123);
            this.member.Name = "member";
            this.member.Size = new System.Drawing.Size(184, 22);
            this.member.TabIndex = 18;
            // 
            // bug_name
            // 
            this.bug_name.Location = new System.Drawing.Point(180, 79);
            this.bug_name.Name = "bug_name";
            this.bug_name.Size = new System.Drawing.Size(184, 22);
            this.bug_name.TabIndex = 17;
            // 
            // bug_code_col
            // 
            this.bug_code_col.Location = new System.Drawing.Point(390, 79);
            this.bug_code_col.MinimumSize = new System.Drawing.Size(22, 20);
            this.bug_code_col.Name = "bug_code_col";
            this.bug_code_col.Size = new System.Drawing.Size(520, 334);
            this.bug_code_col.TabIndex = 34;
            // 
            // mod_code
            // 
            this.mod_code.Location = new System.Drawing.Point(932, 78);
            this.mod_code.Multiline = true;
            this.mod_code.Name = "mod_code";
            this.mod_code.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.mod_code.Size = new System.Drawing.Size(519, 334);
            this.mod_code.TabIndex = 33;
            this.mod_code.WordWrap = false;
            // 
            // download_code
            // 
            this.download_code.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.download_code.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.download_code.Location = new System.Drawing.Point(942, 427);
            this.download_code.Name = "download_code";
            this.download_code.Size = new System.Drawing.Size(154, 28);
            this.download_code.TabIndex = 35;
            this.download_code.Text = "Download Code";
            this.download_code.UseVisualStyleBackColor = false;
            this.download_code.Click += new System.EventHandler(this.download_code_Click);
            // 
            // mod_desc
            // 
            this.mod_desc.Location = new System.Drawing.Point(390, 488);
            this.mod_desc.Multiline = true;
            this.mod_desc.Name = "mod_desc";
            this.mod_desc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.mod_desc.Size = new System.Drawing.Size(1060, 77);
            this.mod_desc.TabIndex = 36;
            this.mod_desc.WordWrap = false;
            // 
            // reason
            // 
            this.reason.Location = new System.Drawing.Point(57, 532);
            this.reason.Multiline = true;
            this.reason.Name = "reason";
            this.reason.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.reason.Size = new System.Drawing.Size(290, 77);
            this.reason.TabIndex = 37;
            this.reason.WordWrap = false;
            // 
            // status
            // 
            this.status.FormattingEnabled = true;
            this.status.Location = new System.Drawing.Point(212, 478);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(136, 24);
            this.status.TabIndex = 38;
            // 
            // submit_query
            // 
            this.submit_query.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.submit_query.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.submit_query.Location = new System.Drawing.Point(354, 571);
            this.submit_query.Name = "submit_query";
            this.submit_query.Size = new System.Drawing.Size(192, 38);
            this.submit_query.TabIndex = 39;
            this.submit_query.Text = "Submit Query";
            this.submit_query.UseVisualStyleBackColor = false;
            this.submit_query.Click += new System.EventHandler(this.submit_query_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(715, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(442, 58);
            this.label1.TabIndex = 40;
            this.label1.Text = "Bug Solution";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(252, 449);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 17);
            this.label10.TabIndex = 41;
            this.label10.Text = "Status";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(54, 512);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 17);
            this.label11.TabIndex = 42;
            this.label11.Text = "Reason";
            // 
            // bugcode
            // 
            this.bugcode.Location = new System.Drawing.Point(390, 79);
            this.bugcode.Multiline = true;
            this.bugcode.Name = "bugcode";
            this.bugcode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.bugcode.Size = new System.Drawing.Size(519, 334);
            this.bugcode.TabIndex = 43;
            this.bugcode.WordWrap = false;
            // 
            // mod_code_col
            // 
            this.mod_code_col.Location = new System.Drawing.Point(932, 78);
            this.mod_code_col.MinimumSize = new System.Drawing.Size(22, 20);
            this.mod_code_col.Name = "mod_code_col";
            this.mod_code_col.Size = new System.Drawing.Size(520, 334);
            this.mod_code_col.TabIndex = 44;
            // 
            // bug_audit
            // 
            this.bug_audit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.bug_audit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bug_audit.Location = new System.Drawing.Point(1259, 571);
            this.bug_audit.Name = "bug_audit";
            this.bug_audit.Size = new System.Drawing.Size(192, 38);
            this.bug_audit.TabIndex = 45;
            this.bug_audit.Text = "Bug Audit";
            this.bug_audit.UseVisualStyleBackColor = false;
            this.bug_audit.Click += new System.EventHandler(this.bug_audit_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(387, 449);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 17);
            this.label12.TabIndex = 46;
            this.label12.Text = "Fixers Description";
            // 
            // BugSolution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1488, 642);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.bug_audit);
            this.Controls.Add(this.mod_code_col);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.submit_query);
            this.Controls.Add(this.status);
            this.Controls.Add(this.reason);
            this.Controls.Add(this.mod_desc);
            this.Controls.Add(this.download_code);
            this.Controls.Add(this.bug_code_col);
            this.Controls.Add(this.mod_code);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Proj);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.date_posted);
            this.Controls.Add(this.op);
            this.Controls.Add(this.os);
            this.Controls.Add(this.code);
            this.Controls.Add(this.member);
            this.Controls.Add(this.bug_name);
            this.Controls.Add(this.bugcode);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "BugSolution";
            this.Text = "BugSolution";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Proj;
        private System.Windows.Forms.TextBox priority;
        private System.Windows.Forms.TextBox date_posted;
        private System.Windows.Forms.TextBox op;
        private System.Windows.Forms.TextBox os;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox member;
        private System.Windows.Forms.TextBox bug_name;
        private System.Windows.Forms.WebBrowser bug_code_col;
        private System.Windows.Forms.TextBox mod_code;
        private System.Windows.Forms.Button download_code;
        private System.Windows.Forms.TextBox mod_desc;
        private System.Windows.Forms.TextBox reason;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.Button submit_query;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox bugcode;
        private System.Windows.Forms.WebBrowser mod_code_col;
        private System.Windows.Forms.Button bug_audit;
        private System.Windows.Forms.Label label12;
    }
}