﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class NewProject : Form
    {
        /// <summary>
        /// set up of string for the username
        /// </summary>
        public string authenticated_user { get; set; }

        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        /// <param name="membername"></param>
        public NewProject(string membername)
        {
            InitializeComponent();
            this.authenticated_user = membername;
            label4.Text = this.authenticated_user;
            database_connection();
        }

        /// <summary>
        /// initiates sqp connection
        /// </summary>
        SqlConnection mySqlConnection;

        /// <summary>
        /// connection to the database is established
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection.Open();
        }

        /// <summary>
        /// Text boxes are cleared once the action is completed
        /// </summary>
        public void clear_text_boxes()
        {
            projname_input.Text = project_description.Text = "";
        }

        /// <summary>
        /// check whether there are projects in the database with the same name
        /// </summary>
        /// <returns></returns>
        public bool check_input_data()
        {
            bool returnvalue = true;
            String existing_projects = "SELECT COUNT(*) FROM Projects WHERE project_name = '" + projname_input.Text + "'";
            SqlCommand cmd_checkprojects = new SqlCommand(existing_projects, mySqlConnection);
            int projects = (int)cmd_checkprojects.ExecuteScalar();

            if (projects > 0)
            {
                MessageBox.Show("Error: Project name that you added already exists");
                returnvalue = false;
            }

            if (
                string.IsNullOrEmpty(projname_input.Text)
                || string.IsNullOrEmpty(project_description.Text))
            {
                MessageBox.Show("Error: Please check your input values");
                returnvalue = false;
            }
            return (returnvalue);
        }

        /// <summary>
        /// a command is developed to add values into the database
        /// </summary>
        /// <param name="project_name"></param>
        /// <param name="project_description"></param>
        /// <param name="date"></param>
        /// <param name="user_name"></param>
        /// <param name="commandString"></param>
        public void insert_Project(String project_name, String project_description, DateTime date, String user_name, String commandString)
        {

            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@project_name", project_name);
                cmdInsert.Parameters.AddWithValue("@project_description", project_description);
                cmdInsert.Parameters.AddWithValue("@date", date);
                cmdInsert.Parameters.AddWithValue("@user_name", user_name);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// when the create project is clicked data will be added into the projects table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void create_project_Click(object sender, EventArgs e)
        {
            if (check_input_data())
            {
                String command_String = "INSERT INTO Projects(project_name, project_description, date, user_name) VALUES (@project_name, @project_description, @date, @user_name)";

                insert_Project(projname_input.Text, project_description.Text, DateTime.Now, this.authenticated_user, command_String);
                MessageBox.Show("New Project " + projname_input.Text + " created");
                clear_text_boxes();
                this.Close();

            }
        }
    }
}
