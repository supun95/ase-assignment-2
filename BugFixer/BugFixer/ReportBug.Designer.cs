﻿namespace BugFixer
{
    partial class ReportBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.codeuploaded = new System.Windows.Forms.TextBox();
            this.bug_desc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Upload_bug = new System.Windows.Forms.Button();
            this.Submit_Bug = new System.Windows.Forms.Button();
            this.bug_name_input = new System.Windows.Forms.TextBox();
            this.project_input = new System.Windows.Forms.ComboBox();
            this.code_input = new System.Windows.Forms.ComboBox();
            this.severity_input = new System.Windows.Forms.ComboBox();
            this.os_input = new System.Windows.Forms.ComboBox();
            this.priority_input = new System.Windows.Forms.ComboBox();
            this.assign_input = new System.Windows.Forms.ComboBox();
            this.bug_name = new System.Windows.Forms.Label();
            this.Code = new System.Windows.Forms.Label();
            this.line_no = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Severity = new System.Windows.Forms.Label();
            this.OS = new System.Windows.Forms.Label();
            this.priority = new System.Windows.Forms.Label();
            this.assign_user = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lin_input = new System.Windows.Forms.TextBox();
            this.upload_col = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // codeuploaded
            // 
            this.codeuploaded.Location = new System.Drawing.Point(564, 99);
            this.codeuploaded.Multiline = true;
            this.codeuploaded.Name = "codeuploaded";
            this.codeuploaded.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.codeuploaded.Size = new System.Drawing.Size(922, 277);
            this.codeuploaded.TabIndex = 0;
            this.codeuploaded.WordWrap = false;
            // 
            // bug_desc
            // 
            this.bug_desc.Location = new System.Drawing.Point(564, 425);
            this.bug_desc.Multiline = true;
            this.bug_desc.Name = "bug_desc";
            this.bug_desc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bug_desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.bug_desc.Size = new System.Drawing.Size(922, 97);
            this.bug_desc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(653, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 58);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report a Bug";
            // 
            // Upload_bug
            // 
            this.Upload_bug.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Upload_bug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Upload_bug.Location = new System.Drawing.Point(937, 382);
            this.Upload_bug.Name = "Upload_bug";
            this.Upload_bug.Size = new System.Drawing.Size(183, 37);
            this.Upload_bug.TabIndex = 3;
            this.Upload_bug.Text = "Bug Upload";
            this.Upload_bug.UseVisualStyleBackColor = false;
            this.Upload_bug.Click += new System.EventHandler(this.Upload_bug_Click);
            // 
            // Submit_Bug
            // 
            this.Submit_Bug.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Submit_Bug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Submit_Bug.Location = new System.Drawing.Point(564, 528);
            this.Submit_Bug.Name = "Submit_Bug";
            this.Submit_Bug.Size = new System.Drawing.Size(350, 53);
            this.Submit_Bug.TabIndex = 4;
            this.Submit_Bug.Text = "Submit Bug Report";
            this.Submit_Bug.UseVisualStyleBackColor = false;
            this.Submit_Bug.Click += new System.EventHandler(this.Submit_Bug_Click);
            // 
            // bug_name_input
            // 
            this.bug_name_input.Location = new System.Drawing.Point(253, 105);
            this.bug_name_input.Name = "bug_name_input";
            this.bug_name_input.Size = new System.Drawing.Size(157, 22);
            this.bug_name_input.TabIndex = 5;
            // 
            // project_input
            // 
            this.project_input.FormattingEnabled = true;
            this.project_input.Location = new System.Drawing.Point(253, 158);
            this.project_input.Name = "project_input";
            this.project_input.Size = new System.Drawing.Size(157, 24);
            this.project_input.TabIndex = 6;
            // 
            // code_input
            // 
            this.code_input.FormattingEnabled = true;
            this.code_input.Location = new System.Drawing.Point(253, 217);
            this.code_input.Name = "code_input";
            this.code_input.Size = new System.Drawing.Size(157, 24);
            this.code_input.TabIndex = 7;
            // 
            // severity_input
            // 
            this.severity_input.FormattingEnabled = true;
            this.severity_input.Location = new System.Drawing.Point(253, 332);
            this.severity_input.Name = "severity_input";
            this.severity_input.Size = new System.Drawing.Size(157, 24);
            this.severity_input.TabIndex = 9;
            // 
            // os_input
            // 
            this.os_input.FormattingEnabled = true;
            this.os_input.Location = new System.Drawing.Point(253, 388);
            this.os_input.Name = "os_input";
            this.os_input.Size = new System.Drawing.Size(157, 24);
            this.os_input.TabIndex = 10;
            // 
            // priority_input
            // 
            this.priority_input.FormattingEnabled = true;
            this.priority_input.Location = new System.Drawing.Point(253, 442);
            this.priority_input.Name = "priority_input";
            this.priority_input.Size = new System.Drawing.Size(157, 24);
            this.priority_input.TabIndex = 11;
            // 
            // assign_input
            // 
            this.assign_input.FormattingEnabled = true;
            this.assign_input.Location = new System.Drawing.Point(253, 495);
            this.assign_input.Name = "assign_input";
            this.assign_input.Size = new System.Drawing.Size(157, 24);
            this.assign_input.TabIndex = 12;
            // 
            // bug_name
            // 
            this.bug_name.AutoSize = true;
            this.bug_name.Location = new System.Drawing.Point(79, 110);
            this.bug_name.Name = "bug_name";
            this.bug_name.Size = new System.Drawing.Size(80, 17);
            this.bug_name.TabIndex = 13;
            this.bug_name.Text = "File Name";
            // 
            // Code
            // 
            this.Code.AutoSize = true;
            this.Code.Location = new System.Drawing.Point(79, 224);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(45, 17);
            this.Code.TabIndex = 14;
            this.Code.Text = "Code";
            // 
            // line_no
            // 
            this.line_no.AutoSize = true;
            this.line_no.Location = new System.Drawing.Point(79, 279);
            this.line_no.Name = "line_no";
            this.line_no.Size = new System.Drawing.Size(100, 17);
            this.line_no.TabIndex = 15;
            this.line_no.Text = "Line Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(79, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Project";
            // 
            // Severity
            // 
            this.Severity.AutoSize = true;
            this.Severity.Location = new System.Drawing.Point(79, 339);
            this.Severity.Name = "Severity";
            this.Severity.Size = new System.Drawing.Size(67, 17);
            this.Severity.TabIndex = 17;
            this.Severity.Text = "Severity";
            // 
            // OS
            // 
            this.OS.AutoSize = true;
            this.OS.Location = new System.Drawing.Point(79, 395);
            this.OS.Name = "OS";
            this.OS.Size = new System.Drawing.Size(137, 17);
            this.OS.TabIndex = 18;
            this.OS.Text = "Operating System";
            // 
            // priority
            // 
            this.priority.AutoSize = true;
            this.priority.Location = new System.Drawing.Point(79, 449);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(60, 17);
            this.priority.TabIndex = 19;
            this.priority.Text = "Priority";
            // 
            // assign_user
            // 
            this.assign_user.AutoSize = true;
            this.assign_user.Location = new System.Drawing.Point(79, 502);
            this.assign_user.Name = "assign_user";
            this.assign_user.Size = new System.Drawing.Size(95, 17);
            this.assign_user.TabIndex = 20;
            this.assign_user.Text = "Assign User";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1386, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 21;
            this.label2.Text = "label2";
            // 
            // lin_input
            // 
            this.lin_input.Location = new System.Drawing.Point(253, 279);
            this.lin_input.Name = "lin_input";
            this.lin_input.Size = new System.Drawing.Size(157, 22);
            this.lin_input.TabIndex = 22;
            // 
            // upload_col
            // 
            this.upload_col.Location = new System.Drawing.Point(564, 99);
            this.upload_col.MinimumSize = new System.Drawing.Size(22, 20);
            this.upload_col.Name = "upload_col";
            this.upload_col.Size = new System.Drawing.Size(922, 277);
            this.upload_col.TabIndex = 23;
            // 
            // ReportBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1500, 593);
            this.Controls.Add(this.upload_col);
            this.Controls.Add(this.lin_input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.assign_user);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.OS);
            this.Controls.Add(this.Severity);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.line_no);
            this.Controls.Add(this.Code);
            this.Controls.Add(this.bug_name);
            this.Controls.Add(this.assign_input);
            this.Controls.Add(this.priority_input);
            this.Controls.Add(this.os_input);
            this.Controls.Add(this.severity_input);
            this.Controls.Add(this.code_input);
            this.Controls.Add(this.project_input);
            this.Controls.Add(this.bug_name_input);
            this.Controls.Add(this.Submit_Bug);
            this.Controls.Add(this.Upload_bug);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bug_desc);
            this.Controls.Add(this.codeuploaded);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "ReportBug";
            this.Text = "ReportBug";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox codeuploaded;
        private System.Windows.Forms.TextBox bug_desc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Upload_bug;
        private System.Windows.Forms.Button Submit_Bug;
        private System.Windows.Forms.TextBox bug_name_input;
        private System.Windows.Forms.ComboBox project_input;
        private System.Windows.Forms.ComboBox code_input;
        private System.Windows.Forms.ComboBox severity_input;
        private System.Windows.Forms.ComboBox os_input;
        private System.Windows.Forms.ComboBox priority_input;
        private System.Windows.Forms.ComboBox assign_input;
        private System.Windows.Forms.Label bug_name;
        private System.Windows.Forms.Label Code;
        private System.Windows.Forms.Label line_no;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Severity;
        private System.Windows.Forms.Label OS;
        private System.Windows.Forms.Label priority;
        private System.Windows.Forms.Label assign_user;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lin_input;
        private System.Windows.Forms.WebBrowser upload_col;
    }
}