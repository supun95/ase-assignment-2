﻿namespace BugFixer
{
    partial class EditUserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.retypePassword_edit = new System.Windows.Forms.TextBox();
            this.Password_edit = new System.Windows.Forms.TextBox();
            this.Email_edit = new System.Windows.Forms.TextBox();
            this.surname_edit = new System.Windows.Forms.TextBox();
            this.firstname_edit = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(189, 385);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(74, 32);
            this.updateButton.TabIndex = 0;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 325);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Retype-Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 17);
            this.label6.TabIndex = 22;
            this.label6.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Surname";
            // 
            // retypePassword_edit
            // 
            this.retypePassword_edit.Location = new System.Drawing.Point(168, 325);
            this.retypePassword_edit.Name = "retypePassword_edit";
            this.retypePassword_edit.Size = new System.Drawing.Size(235, 22);
            this.retypePassword_edit.TabIndex = 19;
            // 
            // Password_edit
            // 
            this.Password_edit.Location = new System.Drawing.Point(168, 275);
            this.Password_edit.Name = "Password_edit";
            this.Password_edit.Size = new System.Drawing.Size(235, 22);
            this.Password_edit.TabIndex = 18;
            // 
            // Email_edit
            // 
            this.Email_edit.Location = new System.Drawing.Point(168, 224);
            this.Email_edit.Name = "Email_edit";
            this.Email_edit.Size = new System.Drawing.Size(235, 22);
            this.Email_edit.TabIndex = 17;
            // 
            // surname_edit
            // 
            this.surname_edit.Location = new System.Drawing.Point(168, 171);
            this.surname_edit.Name = "surname_edit";
            this.surname_edit.Size = new System.Drawing.Size(235, 22);
            this.surname_edit.TabIndex = 16;
            // 
            // firstname_edit
            // 
            this.firstname_edit.Location = new System.Drawing.Point(168, 119);
            this.firstname_edit.Name = "firstname_edit";
            this.firstname_edit.Size = new System.Drawing.Size(235, 22);
            this.firstname_edit.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "First Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 17);
            this.label1.TabIndex = 24;
            this.label1.Text = "Edit User Information";
            // 
            // EditUserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 483);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.retypePassword_edit);
            this.Controls.Add(this.Password_edit);
            this.Controls.Add(this.Email_edit);
            this.Controls.Add(this.surname_edit);
            this.Controls.Add(this.firstname_edit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.updateButton);
            this.Name = "EditUserInfo";
            this.Text = "EditUserInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox retypePassword_edit;
        private System.Windows.Forms.TextBox Password_edit;
        private System.Windows.Forms.TextBox Email_edit;
        private System.Windows.Forms.TextBox surname_edit;
        private System.Windows.Forms.TextBox firstname_edit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}