﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class MyQuestions : Form
    {
        /// <summary>
        /// for the set up of the passed username
        /// </summary>
        public string authenticated_user;

        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        /// <param name="membername"></param>
        public MyQuestions(string membername)
        {
            InitializeComponent();
            database_connection();
            this.authenticated_user = membername;
            infodisplay();
        }

        /// <summary>
        /// calling sql connection
        /// </summary>
        SqlConnection mySqlConnection;

        public String clicked_bug;


        /// <summary>
        /// initiates the database connection
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// when the form starts, all the bugs created by the user will be displayed in the listbox.
        /// </summary>
        public void infodisplay()
        {
            String command = "SELECT file_name FROM Bugs WHERE membername = '" + this.authenticated_user + "'";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                bug_list.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    bug_list.Items.Add(mySqlDataReader["file_name"]);
                }

            }

            catch (SqlException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            mySqlConnection.Close();
        }

        /// <summary>
        /// Displays details of the bugs answered by the particular user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM Bugs WHERE file_name = '" + bug_list.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(command, mySqlConnection);

            try
            {
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    sel_bug.Text = mySqlDataReader2["file_name"].ToString();
                    clicked_bug = mySqlDataReader2["file_name"].ToString();
                    bug_status.Text = mySqlDataReader2["bug_status"].ToString();
                    date.Text = mySqlDataReader2["submit_date"].ToString();
                    code.Text = mySqlDataReader2["code_type"].ToString();
                    severity.Text = mySqlDataReader2["severity"].ToString();                  
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// checks whether the user has selected a file name from the listbox
        /// </summary>
        /// <returns></returns>
        public bool validate_data()
        {
            bool returnvalue = true;

            if (
                string.IsNullOrEmpty(sel_bug.Text))
            {
                MessageBox.Show("Error: Please select a bug from the list");
                returnvalue = false;
            }

            return (returnvalue);

        }

        /// <summary>
        /// when the check bug button is clicked, chosen data will be validate and the bug solutions screen will be displayed 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void check_bug_Click(object sender, EventArgs e)
        {
            if (validate_data())//checks the data validity
            {
                BugSolution newForm = new BugSolution(clicked_bug, this.authenticated_user);
                newForm.Show();
                this.Close();
            }
        }
    }
}
