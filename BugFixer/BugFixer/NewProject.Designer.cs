﻿namespace BugFixer
{
    partial class NewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Description = new System.Windows.Forms.Label();
            this.project_description = new System.Windows.Forms.TextBox();
            this.projname_input = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.create_project = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.Location = new System.Drawing.Point(78, 182);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(90, 17);
            this.Description.TabIndex = 23;
            this.Description.Text = "Description";
            // 
            // project_description
            // 
            this.project_description.Location = new System.Drawing.Point(210, 179);
            this.project_description.Multiline = true;
            this.project_description.Name = "project_description";
            this.project_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.project_description.Size = new System.Drawing.Size(358, 215);
            this.project_description.TabIndex = 18;
            // 
            // projname_input
            // 
            this.projname_input.Location = new System.Drawing.Point(210, 135);
            this.projname_input.Name = "projname_input";
            this.projname_input.Size = new System.Drawing.Size(264, 22);
            this.projname_input.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Project Name";
            // 
            // create_project
            // 
            this.create_project.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.create_project.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.create_project.Location = new System.Drawing.Point(210, 419);
            this.create_project.Name = "create_project";
            this.create_project.Size = new System.Drawing.Size(129, 31);
            this.create_project.TabIndex = 14;
            this.create_project.Text = "Create Project";
            this.create_project.UseVisualStyleBackColor = false;
            this.create_project.Click += new System.EventHandler(this.create_project_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(110, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(411, 58);
            this.label1.TabIndex = 24;
            this.label1.Text = "New Project";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(505, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 25;
            this.label4.Text = "label4";
            // 
            // NewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(601, 495);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.project_description);
            this.Controls.Add(this.projname_input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.create_project);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "NewProject";
            this.Text = "NewProjects";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Description;
        private System.Windows.Forms.TextBox project_description;
        private System.Windows.Forms.TextBox projname_input;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button create_project;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
    }
}