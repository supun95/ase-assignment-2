﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Octokit;
using System.Data.SqlClient;
using ColorCode;
using System.IO;
using System.Net.Mail;

namespace BugFixer
{
    public partial class FixBug : Form
    {
        /// <summary>
        /// obtaining the username and the bug namefrom the previous form
        /// </summary>
        public string passed_bug { get; set; }
        public string authenticated_user { get; set; }

        public String user_email;

        /// <summary>
        /// Relevant methods are initialised when the form loads 
        /// </summary>
        /// <param name="membername"></param>
        /// <param name="bug"></param>
        public FixBug(string membername, string bug)
        {
            InitializeComponent();
            database_connection();
            this.authenticated_user = membername;
            this.passed_bug = bug;
            Start_form();
        }

        /// <summary>
        /// initiates the database collection
        /// </summary>
        SqlConnection mySqlConnection;
        public String solcode;

        /// <summary>
        /// database connection is set up to query to the database via the form
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// obtaining relevant data from the database using the passed bug name from the previous form and setting up the form information when the form loads up. Thus, te user will obtain required information straight away.
        /// </summary>
        public void Start_form()
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM Bugs WHERE file_name = '" + this.passed_bug + "'";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);
            try
            {               
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                while (mySqlDataReader.Read())//populates listboxes accordingly
                {
                    bug_name.Text = mySqlDataReader["file_name"].ToString();
                    member.Text = mySqlDataReader["membername"].ToString();
                    ctype.Text = mySqlDataReader["code_type"].ToString();
                    string sourceCode = mySqlDataReader["uploaded_code"].ToString();
                    string colorizedSourceCode = new CodeColorizer().Colorize(sourceCode, Languages.CSharp);
                    bug_code.DocumentText = colorizedSourceCode;
                    os.Text = mySqlDataReader["os"].ToString();
                    priority.Text = mySqlDataReader["priority"].ToString();
                    date_posted.Text = mySqlDataReader["submit_date"].ToString();
                    bug_desc.Text = mySqlDataReader["bug_desc"].ToString();
                    textBox1.Text = mySqlDataReader["uploaded_code"].ToString();
                    line_no.Text = mySqlDataReader["line_number"].ToString();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();

        }

        /// <summary>
        /// checks validity of user input. Error messages will be displayed if the user is not satisfying this scenario
        /// </summary>
        /// <returns></returns>
        public bool Validate_data()
        {
            bool returnvalue = true;
            if (
                string.IsNullOrEmpty(sol_code.Text)
                || string.IsNullOrEmpty(sol_desc.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                returnvalue = false;
            }
            return (returnvalue);
        }

        /// <summary>
        /// if the user wants to download the code save file dialog will be opened and the user will be able to save the source code in a folder that they desire. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void download_code_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox1.Text);
            }
        }

        /// <summary>
        /// if the user wants to upload the code save file dialog will be opened and the user will be able to upload relelvant source code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void upload_code_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                sol_code.Text = System.IO.File.ReadAllText(openFileDialog1.FileName);
                solcode = System.IO.File.ReadAllText(openFileDialog1.FileName);
                string solutioncodetext = sol_code.Text;
                solutioncodetext = System.IO.File.ReadAllText(openFileDialog1.FileName);
                string colorizedSourceCode = new CodeColorizer().Colorize(solutioncodetext, Languages.CSharp);
                col_code.DocumentText = colorizedSourceCode;
            }
        }

        /// <summary>
        /// preparing values to insert bug informations in the bugs database 
        /// </summary>
        /// <param name="fixed_code"></param>
        /// <param name="fix_desc"></param>
        /// <param name="bug_status"></param>
        /// <param name="fix_date"></param>
        /// <param name="fixer_name"></param>
        /// <param name="file_name"></param>
        /// <param name="commandString"></param>
        public void insertquery(String fixed_code, String fix_desc, String bug_status, DateTime fix_date, String fixer_name, String file_name, String commandString)
        {
            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@fixed_code", fixed_code);
                cmdInsert.Parameters.AddWithValue("@fix_desc", fix_desc);
                cmdInsert.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert.Parameters.AddWithValue("@fix_date", fix_date);
                cmdInsert.Parameters.AddWithValue("@fixer_name", fixer_name);
                cmdInsert.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// preparing values to insert into the bugaudit database
        /// </summary>
        /// <param name="file_name"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="membername"></param>
        /// <param name="fixers_name"></param>
        /// <param name="bug_status"></param>
        /// <param name="initial_code"></param>
        /// <param name="final_code"></param>
        /// <param name="code_type"></param>
        /// <param name="commandString2"></param>
        public void insertbugAudit(String file_name, String description, DateTime date, String membername, String fixers_name, String bug_status, String initial_code, String final_code, String code_type, String commandString2)
        {
            try
            {
                SqlCommand cmdInsert2 = new SqlCommand(commandString2, mySqlConnection);
                cmdInsert2.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@membername", membername);
                cmdInsert2.Parameters.AddWithValue("@fixers_name", fixers_name);
                cmdInsert2.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert2.Parameters.AddWithValue("@initial_code", initial_code);
                cmdInsert2.Parameters.AddWithValue("@final_code", final_code);
                cmdInsert2.Parameters.AddWithValue("@code_type", code_type);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// submission of bug information into the bug and bugaudit database 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void submit_sol_Click(object sender, EventArgs e)
        {           
            if (Validate_data())
            {
                //connectiong with github
                var ghClient = new GitHubClient(new ProductHeaderValue("Bug_Creation"));
                ghClient.Credentials = new Credentials("c7efc3c1d1459f6e7417352f3e30ee9e4814b4c8");
                String filename = bug_name.ToString();
                // github variables
                var owner = "supun95";
                var repo = "repo1";
                var branch = "master";
                var targetFile = "path/" + filename;
                var code = sol_code.Text;
                var existingFile = await ghClient.Repository.Content.GetAllContentsByRef(owner, repo, targetFile, branch);

                // update the file
                var updateChangeSet = await ghClient.Repository.Content.UpdateFile(owner, repo, targetFile,
                   new UpdateFileRequest("BugFixer File update", code + DateTime.UtcNow, existingFile.First().Sha, branch));

                mySqlConnection.Open();
                String commandString = "UPDATE Bugs SET fixed_code = @fixed_code, fix_desc = @fix_desc, bug_status = @bug_status, fix_date = @fix_date, fixer_name = @fixer_name WHERE file_name = @file_name";
                insertquery(sol_code.Text, sol_desc.Text, "Pending Approval", DateTime.Now, this.authenticated_user, this.passed_bug, commandString);

                String commandString2 = "INSERT INTO BugAudit(file_name, description, date, membername, fixers_name, bug_status, initial_code, final_code, code_type) VALUES (@file_name, @description, @date, @membername, @fixers_name, @bug_status, @initial_code, @final_code, @code_type)";
                insertbugAudit(this.passed_bug, sol_desc.Text, DateTime.Now, member.Text, this.authenticated_user, "Pending Approval", textBox1.Text, sol_code.Text, ctype.Text, commandString2);


                MessageBox.Show("Your bug has been submitted");

                if (email_check.Checked)//if email notification is required
                {
                    String selctemail = "SELECT * FROM users WHERE user_name = '" + member.Text + "'";
                    SqlCommand mySqlCommand = new SqlCommand(selctemail, mySqlConnection);
                    try
                    {
                        SqlDataReader mySqlDataReader5 = mySqlCommand.ExecuteReader();
                        while (mySqlDataReader5.Read())
                        {
                            user_email = mySqlDataReader5["user_email"].ToString();
                        }
                    }

                    catch (SqlException ex)
                    {

                        MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    mySqlConnection.Close();

                   
                    ///sends an email to the user showing notifications and solutions
                    SmtpClient client = new SmtpClient();
                    client.Port = 587;
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential("stackquestionsme@gmail.com", "110195sd");
                    string repoaddress = "https://github.com/supun95/repo1";

                    MailMessage mm = new MailMessage("stackquestionsme@gmail.com", user_email, "BugFixed", "Your bug '" + this.passed_bug + "' was fixed by user: " + this.authenticated_user + Environment.NewLine + "Log into BugFixer to accept the answer" + Environment.NewLine + Environment.NewLine + "Reply to him/her: " + user_email + Environment.NewLine + "Repository Access: "+repoaddress);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mm);
                    MessageBox.Show("Email Sent");
                    this.Close();
                }
              }
            }
    }
}
