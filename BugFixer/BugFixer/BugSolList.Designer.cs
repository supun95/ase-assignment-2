﻿namespace BugFixer
{
    partial class BugSolList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bug_list = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.TextBox();
            this.severity = new System.Windows.Forms.TextBox();
            this.bug_status = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sel_bug = new System.Windows.Forms.TextBox();
            this.bug_sol = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bug_list
            // 
            this.bug_list.FormattingEnabled = true;
            this.bug_list.ItemHeight = 16;
            this.bug_list.Location = new System.Drawing.Point(106, 106);
            this.bug_list.Name = "bug_list";
            this.bug_list.ScrollAlwaysVisible = true;
            this.bug_list.Size = new System.Drawing.Size(292, 388);
            this.bug_list.TabIndex = 2;
            this.bug_list.SelectedIndexChanged += new System.EventHandler(this.bug_list_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(496, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Date Modified";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(513, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Code Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(533, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 34;
            this.label3.Text = "Severity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(516, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "Bug Status";
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(606, 299);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(316, 22);
            this.date.TabIndex = 32;
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(606, 254);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(316, 22);
            this.code.TabIndex = 31;
            // 
            // severity
            // 
            this.severity.Location = new System.Drawing.Point(606, 208);
            this.severity.Name = "severity";
            this.severity.Size = new System.Drawing.Size(316, 22);
            this.severity.TabIndex = 30;
            // 
            // bug_status
            // 
            this.bug_status.Location = new System.Drawing.Point(606, 164);
            this.bug_status.Name = "bug_status";
            this.bug_status.Size = new System.Drawing.Size(316, 22);
            this.bug_status.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(496, 346);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 38;
            this.label1.Text = "Selected Bug";
            // 
            // sel_bug
            // 
            this.sel_bug.Location = new System.Drawing.Point(606, 343);
            this.sel_bug.Name = "sel_bug";
            this.sel_bug.Size = new System.Drawing.Size(316, 22);
            this.sel_bug.TabIndex = 37;
            // 
            // bug_sol
            // 
            this.bug_sol.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.bug_sol.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bug_sol.Location = new System.Drawing.Point(519, 462);
            this.bug_sol.Name = "bug_sol";
            this.bug_sol.Size = new System.Drawing.Size(207, 32);
            this.bug_sol.TabIndex = 39;
            this.bug_sol.Text = "Check Bug Audit";
            this.bug_sol.UseVisualStyleBackColor = false;
            this.bug_sol.Click += new System.EventHandler(this.bug_sol_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(273, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(649, 58);
            this.label6.TabIndex = 40;
            this.label6.Text = "Check Bug Solution";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(102, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = "Bugs";
            // 
            // BugSolList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1066, 547);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bug_sol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sel_bug);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.date);
            this.Controls.Add(this.code);
            this.Controls.Add(this.severity);
            this.Controls.Add(this.bug_status);
            this.Controls.Add(this.bug_list);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "BugSolList";
            this.Text = "BugSolList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox bug_list;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox date;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox severity;
        private System.Windows.Forms.TextBox bug_status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox sel_bug;
        private System.Windows.Forms.Button bug_sol;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}