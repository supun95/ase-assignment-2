﻿namespace BugFixer
{
    partial class MyProjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.p_desc = new System.Windows.Forms.TextBox();
            this.p_list = new System.Windows.Forms.ListBox();
            this.bug_list = new System.Windows.Forms.ListBox();
            this.p_name = new System.Windows.Forms.TextBox();
            this.p_date = new System.Windows.Forms.TextBox();
            this.bug_hist = new System.Windows.Forms.Button();
            this.selec_bug = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.use_create = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // p_desc
            // 
            this.p_desc.Location = new System.Drawing.Point(508, 335);
            this.p_desc.Multiline = true;
            this.p_desc.Name = "p_desc";
            this.p_desc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.p_desc.Size = new System.Drawing.Size(681, 153);
            this.p_desc.TabIndex = 19;
            this.p_desc.WordWrap = false;
            // 
            // p_list
            // 
            this.p_list.FormattingEnabled = true;
            this.p_list.HorizontalScrollbar = true;
            this.p_list.ItemHeight = 16;
            this.p_list.Location = new System.Drawing.Point(28, 148);
            this.p_list.Name = "p_list";
            this.p_list.ScrollAlwaysVisible = true;
            this.p_list.Size = new System.Drawing.Size(180, 340);
            this.p_list.TabIndex = 20;
            this.p_list.SelectedIndexChanged += new System.EventHandler(this.p_list_SelectedIndexChanged);
            // 
            // bug_list
            // 
            this.bug_list.FormattingEnabled = true;
            this.bug_list.HorizontalScrollbar = true;
            this.bug_list.ItemHeight = 16;
            this.bug_list.Location = new System.Drawing.Point(230, 148);
            this.bug_list.Name = "bug_list";
            this.bug_list.ScrollAlwaysVisible = true;
            this.bug_list.Size = new System.Drawing.Size(180, 340);
            this.bug_list.TabIndex = 21;
            this.bug_list.SelectedIndexChanged += new System.EventHandler(this.bug_list_SelectedIndexChanged);
            // 
            // p_name
            // 
            this.p_name.Location = new System.Drawing.Point(508, 168);
            this.p_name.Name = "p_name";
            this.p_name.Size = new System.Drawing.Size(681, 22);
            this.p_name.TabIndex = 22;
            // 
            // p_date
            // 
            this.p_date.Location = new System.Drawing.Point(508, 222);
            this.p_date.Name = "p_date";
            this.p_date.Size = new System.Drawing.Size(681, 22);
            this.p_date.TabIndex = 23;
            // 
            // bug_hist
            // 
            this.bug_hist.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.bug_hist.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bug_hist.Location = new System.Drawing.Point(565, 506);
            this.bug_hist.Name = "bug_hist";
            this.bug_hist.Size = new System.Drawing.Size(165, 41);
            this.bug_hist.TabIndex = 24;
            this.bug_hist.Text = "Bug Audit";
            this.bug_hist.UseVisualStyleBackColor = false;
            this.bug_hist.Click += new System.EventHandler(this.bug_hist_Click);
            // 
            // selec_bug
            // 
            this.selec_bug.Location = new System.Drawing.Point(230, 515);
            this.selec_bug.Name = "selec_bug";
            this.selec_bug.Size = new System.Drawing.Size(180, 22);
            this.selec_bug.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "Project";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(288, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 27;
            this.label2.Text = "Bug List";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 491);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 28;
            this.label3.Text = "Selected Bug";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(505, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 29;
            this.label4.Text = "Project ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(505, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 17);
            this.label5.TabIndex = 30;
            this.label5.Text = "Date Created";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(505, 312);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 31;
            this.label6.Text = "Description";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(476, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(399, 58);
            this.label7.TabIndex = 32;
            this.label7.Text = "My Projects";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(505, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 17);
            this.label8.TabIndex = 34;
            this.label8.Text = "Created By";
            // 
            // use_create
            // 
            this.use_create.Location = new System.Drawing.Point(508, 278);
            this.use_create.Name = "use_create";
            this.use_create.Size = new System.Drawing.Size(681, 22);
            this.use_create.TabIndex = 33;
            // 
            // MyProjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1239, 569);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.use_create);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selec_bug);
            this.Controls.Add(this.bug_hist);
            this.Controls.Add(this.p_date);
            this.Controls.Add(this.p_name);
            this.Controls.Add(this.bug_list);
            this.Controls.Add(this.p_list);
            this.Controls.Add(this.p_desc);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "MyProjects";
            this.Text = "My Projects";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox p_desc;
        private System.Windows.Forms.ListBox p_list;
        private System.Windows.Forms.ListBox bug_list;
        private System.Windows.Forms.TextBox p_name;
        private System.Windows.Forms.TextBox p_date;
        private System.Windows.Forms.Button bug_hist;
        private System.Windows.Forms.TextBox selec_bug;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox use_create;
    }
}