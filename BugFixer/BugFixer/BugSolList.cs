﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugFixer
{
    public partial class BugSolList : Form
    {
        /// <summary>
        /// obtaining the username from the previous form
        /// </summary>
        public string authenticated_user { get; set; }

        /// <summary>
        /// all the methods are initiated at the start
        /// </summary>
        /// <param name="membername"></param>
        public BugSolList(string membername)
        {
            InitializeComponent();
            database_connection();
            this.authenticated_user = membername;
            infodisplay();
        }

        /// <summary>
        /// initiates the database connection
        /// </summary>
        SqlConnection mySqlConnection;
 
        public String clicked_bug;

        /// <summary>
        /// initiates the database connection
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// when the user selects an item from the listbox, all the information about the particular bug is obtained from the database and populated in the textboxes
        /// </summary>
        public void infodisplay()
        {
            String command = "SELECT file_name FROM Bugs WHERE fixer_name = '" + this.authenticated_user + "'";
            SqlCommand mySqlCommand = new SqlCommand(command, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                bug_list.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    bug_list.Items.Add(mySqlDataReader["file_name"]);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// Displays details of the bugs answered by the particular user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            mySqlConnection.Open();
            String command = "SELECT * FROM Bugs WHERE file_name = '" + bug_list.Text + "'";
            SqlCommand mySqlCommand2 = new SqlCommand(command, mySqlConnection);
            try
            {               
                SqlDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bug_status.Text = mySqlDataReader2["bug_status"].ToString();
                    severity.Text = mySqlDataReader2["severity"].ToString();
                    code.Text = mySqlDataReader2["code_type"].ToString();
                    date.Text = mySqlDataReader2["fix_date"].ToString();
                    sel_bug.Text = mySqlDataReader2["file_name"].ToString();
                    clicked_bug = mySqlDataReader2["file_name"].ToString(); 
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// checks whether the user has selected a file name from the listbox. If not, an error message will be displayed in order to validate data
        /// </summary>
        /// <returns></returns>
        public bool validate_data()
        {
            bool returnvalue = true;
            if (
                string.IsNullOrEmpty(sel_bug.Text))
            {
                MessageBox.Show("Error: Please select a bug from the list");
                returnvalue = false;
            }

            return (returnvalue);

        }

        /// <summary>
        /// the button guides the user to the bugaudit form. It passes both the chosen bug name and the username
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_sol_Click(object sender, EventArgs e)
        {
            if (validate_data())//checks the data validity
            {
                BugAudit newForm = new BugAudit(clicked_bug, this.authenticated_user);
                newForm.Show();
                this.Close();
            }
        }       
    }
}
