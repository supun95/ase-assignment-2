﻿namespace BugFixer
{
    partial class MyQuestions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bug_list = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bug_status = new System.Windows.Forms.TextBox();
            this.severity = new System.Windows.Forms.TextBox();
            this.date = new System.Windows.Forms.TextBox();
            this.sel_bug = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.check_bug = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "My Queries";
            // 
            // bug_list
            // 
            this.bug_list.FormattingEnabled = true;
            this.bug_list.HorizontalScrollbar = true;
            this.bug_list.ItemHeight = 16;
            this.bug_list.Location = new System.Drawing.Point(36, 133);
            this.bug_list.Name = "bug_list";
            this.bug_list.ScrollAlwaysVisible = true;
            this.bug_list.Size = new System.Drawing.Size(310, 372);
            this.bug_list.TabIndex = 27;
            this.bug_list.SelectedIndexChanged += new System.EventHandler(this.bug_list_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 449);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Selected Bug";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 369);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "Date Posted";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(385, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 31;
            this.label4.Text = "Severity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(385, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 17);
            this.label5.TabIndex = 32;
            this.label5.Text = "Bug Status";
            // 
            // bug_status
            // 
            this.bug_status.Location = new System.Drawing.Point(388, 157);
            this.bug_status.Name = "bug_status";
            this.bug_status.Size = new System.Drawing.Size(240, 22);
            this.bug_status.TabIndex = 33;
            // 
            // severity
            // 
            this.severity.Location = new System.Drawing.Point(388, 229);
            this.severity.Name = "severity";
            this.severity.Size = new System.Drawing.Size(240, 22);
            this.severity.TabIndex = 34;
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(388, 389);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(240, 22);
            this.date.TabIndex = 35;
            // 
            // sel_bug
            // 
            this.sel_bug.Location = new System.Drawing.Point(388, 469);
            this.sel_bug.Name = "sel_bug";
            this.sel_bug.Size = new System.Drawing.Size(240, 22);
            this.sel_bug.TabIndex = 36;
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(388, 306);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(240, 22);
            this.code.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(385, 286);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "Code";
            // 
            // check_bug
            // 
            this.check_bug.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.check_bug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.check_bug.Location = new System.Drawing.Point(244, 533);
            this.check_bug.Name = "check_bug";
            this.check_bug.Size = new System.Drawing.Size(165, 41);
            this.check_bug.TabIndex = 39;
            this.check_bug.Text = "Check My Query";
            this.check_bug.UseVisualStyleBackColor = false;
            this.check_bug.Click += new System.EventHandler(this.check_bug_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(135, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(440, 58);
            this.label7.TabIndex = 40;
            this.label7.Text = "My Questions";
            // 
            // MyQuestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(642, 608);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.check_bug);
            this.Controls.Add(this.code);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sel_bug);
            this.Controls.Add(this.date);
            this.Controls.Add(this.severity);
            this.Controls.Add(this.bug_status);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bug_list);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "MyQuestions";
            this.Text = "MyQuestions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox bug_list;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bug_status;
        private System.Windows.Forms.TextBox severity;
        private System.Windows.Forms.TextBox date;
        private System.Windows.Forms.TextBox sel_bug;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button check_bug;
        private System.Windows.Forms.Label label7;
    }
}