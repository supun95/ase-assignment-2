﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Octokit;
using System.Net.Mail;
using System.Data.SqlClient;
using ColorCode;


namespace BugFixer
{
    public partial class ReportBug : Form
    {
        /// <summary>
        /// to pass the username from the previous form
        /// </summary>
        public string authenticated_user { get; set; }
        public String useremail;
        public String replyemail;
        public String code_uploaded;

        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        /// <param name="membername"></param>
        public ReportBug(string membername)
        {
            InitializeComponent();
            this.authenticated_user = membername;
            label2.Text = this.authenticated_user;
            database_connection();
            droplistitems();
            Existing_Projects();
            member_bug_assign();
        }

        /// <summary>
        /// initiates sql connection
        /// </summary>
        SqlConnection mySqlConnection;

        /// <summary>
        /// Setting up the data connection
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// Obtain exisitng memebers from the database and populate assign user listbox
        /// </summary>
        public void member_bug_assign()
        {
            mySqlConnection.Open();
            String select_user = "SELECT * FROM users";
            SqlCommand SqlCommand = new SqlCommand(select_user, mySqlConnection);
            try
            {
                SqlDataReader Data_reader = SqlCommand.ExecuteReader();

                while (Data_reader.Read())
                {
                    assign_input.Items.Add(Data_reader["user_name"]);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mySqlConnection.Close();
        }

        /// <summary>
        /// Obtain exisitng projects from the database and populate project listbox
        /// </summary>
        public void Existing_Projects()
        {
            mySqlConnection.Open();
            String project_select = "SELECT * FROM Projects";
            SqlCommand mySqlCommand2 = new SqlCommand(project_select, mySqlConnection);
            try
            {
                SqlDataReader Data_reader = mySqlCommand2.ExecuteReader();
                while (Data_reader.Read())
                {
                    project_input.Items.Add(Data_reader["project_name"]);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            mySqlConnection.Close();
        }

        /// <summary>
        /// clear all the input boxes once the form is submitted
        /// </summary>
        public void input_clear()
        {
            bug_name_input.Text = project_input.Text = line_no.Text = code_input.Text = code_input.Text = os_input.Text = project_input.Text = assign_input.Text = project_input.Text = bug_desc.Text = codeuploaded.Text = "";
        }

        /// <summary>
        /// Adding data into drop down list
        /// </summary>
        public void droplistitems()
        {
            code_input.Items.Add("C#");
            code_input.Items.Add("C++");
            code_input.Items.Add("C");
            code_input.Items.Add("VB");
            code_input.Items.Add("Oracle");
            code_input.Items.Add("CSS");
            code_input.Items.Add("Ruby");
            code_input.Items.Add("MySQL");
            code_input.Items.Add("Perl");
            code_input.Items.Add("JavaScript");
            code_input.Items.Add("Python");
            code_input.Items.Add("Java");
            code_input.Items.Add("Android");
            code_input.Items.Add("PHP");
            code_input.Items.Add("HTML");
            code_input.Items.Add("Other");

            severity_input.Items.Add("Critical");
            severity_input.Items.Add("Medium");
            severity_input.Items.Add("Minor");            

            os_input.Items.Add("Microsoft");
            os_input.Items.Add("Android");
            os_input.Items.Add("Linux");
            os_input.Items.Add("Apple OS");
            os_input.Items.Add("Other");
            os_input.Items.Add("All");

            priority_input.Items.Add("High Priority");
            priority_input.Items.Add("Medium Priority");
            priority_input.Items.Add("Low Priority");            

        }

        /// <summary>
        /// input value validation procedure
        /// </summary>
        /// <returns></returns>
        public bool Input_validation()
        {
            mySqlConnection.Open();
            bool returnvalue = true;
            String checkextbug = "SELECT COUNT(*) FROM Bugs WHERE file_name = '" + bug_name_input.Text + "'";
            SqlCommand check_bug = new SqlCommand(checkextbug, mySqlConnection);
            int bug_count = (int)check_bug.ExecuteScalar();

            //check if the bug name exists
            if (bug_count > 0)
            {
                MessageBox.Show("Error: Your assigned bug name already exists. Bug name must be unique");
                returnvalue = false;
            }
            mySqlConnection.Close();

            if (
                string.IsNullOrEmpty(bug_name_input.Text)
                || string.IsNullOrEmpty(project_input.Text)
                || string.IsNullOrEmpty(code_input.Text)
                || string.IsNullOrEmpty(line_no.Text)
                || string.IsNullOrEmpty(severity_input.Text)              
                || string.IsNullOrEmpty(os_input.Text)
                || string.IsNullOrEmpty(priority_input.Text)
                || string.IsNullOrEmpty(assign_user.Text)
                || string.IsNullOrEmpty(codeuploaded.Text)
                || string.IsNullOrEmpty(bug_desc.Text))
            {
                MessageBox.Show("Error: Input Values cannot be EMPTY!");
                returnvalue = false;
            }
            return (returnvalue);         
        }

        /// <summary>
        /// using open file dialog to obtain a file to upload code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Upload_bug_Click(object sender, EventArgs e)
        {
            OpenFileDialog uploadCode = new OpenFileDialog();
            if (uploadCode.ShowDialog() == DialogResult.OK)
            {
                codeuploaded.Text = System.IO.File.ReadAllText(uploadCode.FileName);
                code_uploaded = System.IO.File.ReadAllText(uploadCode.FileName);
                string bugCode = System.IO.File.ReadAllText(uploadCode.FileName);
                string colorizedmyCode = new CodeColorizer().Colorize(bugCode, Languages.CSharp);
                upload_col.DocumentText = colorizedmyCode;
            }
        }


        /// <summary>
        /// prparing values in order to insert them into the database 
        /// </summary>
        /// <param name="file_name"></param>
        /// <param name="severity"></param>
        /// <param name="line_number"></param>
        /// <param name="os"></param>
        /// <param name="priority"></param>
        /// <param name="bug_desc"></param>
        /// <param name="uploaded_code"></param>
        /// <param name="membername"></param>
        /// <param name="bug_status"></param>
        /// <param name="submit_date"></param>
        /// <param name="code_type"></param>
        /// <param name="project_name"></param>
        /// <param name="assignuser"></param>
        /// <param name="commandString"></param>
        public void insertQuery(String file_name, String severity, String line_number, String os, String priority, String bug_desc, String uploaded_code, String membername, String bug_status, DateTime submit_date, String code_type, String project_name, String assignuser, String commandString)
        {

            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert.Parameters.AddWithValue("@severity", severity);
                cmdInsert.Parameters.AddWithValue("@line_number", line_number);
                cmdInsert.Parameters.AddWithValue("@os", os);
                cmdInsert.Parameters.AddWithValue("@priority", priority);
                cmdInsert.Parameters.AddWithValue("@bug_desc", bug_desc);
                cmdInsert.Parameters.AddWithValue("@uploaded_code", uploaded_code);
                cmdInsert.Parameters.AddWithValue("@membername", membername);
                cmdInsert.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert.Parameters.AddWithValue("@submit_date", submit_date);
                cmdInsert.Parameters.AddWithValue("@code_type", code_type);
                cmdInsert.Parameters.AddWithValue("@project_name", project_name);
                cmdInsert.Parameters.AddWithValue("@assignuser", assignuser);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// prparing values in order to insert them into the database 
        /// </summary>
        /// <param name="file_name"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="membername"></param>
        /// <param name="bug_status"></param>
        /// <param name="initial_code"></param>
        /// <param name="final_code"></param>
        /// <param name="code_type"></param>
        /// <param name="commandString2"></param>
        public void insertQueryAudit(String file_name, String description, DateTime date, String membername, String bug_status, String initial_code, String final_code, String code_type, String commandString2)
        {

            try
            {
                SqlCommand cmdInsert2 = new SqlCommand(commandString2, mySqlConnection);

                cmdInsert2.Parameters.AddWithValue("@file_name", file_name);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@membername", membername);
                cmdInsert2.Parameters.AddWithValue("@bug_status", bug_status);
                cmdInsert2.Parameters.AddWithValue("@initial_code", initial_code);
                cmdInsert2.Parameters.AddWithValue("@final_code", final_code);
                cmdInsert2.Parameters.AddWithValue("@code_type", code_type);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// inserting data into the database for bugs and bugaudit. Data are validated before added to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Submit_Bug_Click(object sender, EventArgs e)
        {
            if (Input_validation())
            {
                mySqlConnection.Open();
                String commandString = "INSERT INTO Bugs(file_name, severity, line_number, os, priority, bug_desc, uploaded_code, membername, bug_status, submit_date, code_type, project_name, assignuser) VALUES (@file_name, @severity, @line_number, @os, @priority, @bug_desc, @uploaded_code, @membername, @bug_status, @submit_date, @code_type, @project_name, @assignuser)";
                insertQuery(bug_name_input.Text, severity_input.Text, lin_input.Text, os_input.Text, priority_input.Text, bug_desc.Text, codeuploaded.Text, this.authenticated_user, "Initial Query", DateTime.Now, code_input.Text, project_input.Text, assign_input.Text, commandString);

                String commandString2 = "INSERT INTO BugAudit(file_name, description, date, membername, bug_status, initial_code, final_code, code_type) VALUES (@file_name, @description, @date, @membername, @bug_status, @initial_code, @final_code, @code_type)";
                insertQueryAudit(bug_name_input.Text, bug_desc.Text, DateTime.Now, this.authenticated_user, "Initial Question", codeuploaded.Text, "The Query is not yet resolved by any member", code_input.Text, commandString2);


                String selctemail = "SELECT * FROM users WHERE user_name = '" + assign_input.Text + "'";
                SqlCommand mySqlCommand = new SqlCommand(selctemail, mySqlConnection);
                try
                {
                    SqlDataReader mySqlDataReader5 = mySqlCommand.ExecuteReader();
                    while (mySqlDataReader5.Read())
                    {
                        useremail = mySqlDataReader5["user_email"].ToString();
                    }
                }

                catch (SqlException ex)
                {

                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                mySqlConnection.Close();

                mySqlConnection.Open();

                String selctemail2 = "SELECT * FROM users WHERE user_name = '" + this.authenticated_user + "'";
                SqlCommand mySqlCommand2 = new SqlCommand(selctemail2, mySqlConnection);
                try
                {
                    SqlDataReader mySqlDataReader6 = mySqlCommand2.ExecuteReader();
                    while (mySqlDataReader6.Read())
                    {
                        replyemail = mySqlDataReader6["user_email"].ToString();
                    }
                }

                catch (SqlException ex)
                {

                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                mySqlConnection.Close();

                //github initiation
                var ghClient = new GitHubClient(new ProductHeaderValue("Bug_Creation"));
                ghClient.Credentials = new Credentials("c7efc3c1d1459f6e7417352f3e30ee9e4814b4c8");
                String filename = bug_name_input.ToString();

                // github variables
                var owner = "supun95";
                var repo = "repo1";
                var branch = "master";
                var targetFile = "path/" + filename;

                // create file
                var createChangeSet = ghClient.Repository.Content.CreateFile(owner, repo, targetFile, new CreateFileRequest("BugFixer File Creation", code_uploaded, branch));


                //An email will be sent to the assigned user showing all the attributes of the bug
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("stackquestionsme@gmail.com", "110195sd");
                string repoaddress = "https://github.com/supun95/repo1";


                //email message
                MailMessage mm = new MailMessage("stackquestionsme@gmail.com", useremail, "Bug Report from BugFixer", "Queried By: " + this.authenticated_user + Environment.NewLine + "Bug Name: " + bug_name_input.Text + Environment.NewLine + "Code Type: " + code_input.Text + Environment.NewLine + "Project: " + project_input.Text + Environment.NewLine + "Severity: " + severity_input.Text + Environment.NewLine + "Operating System: " + OS.Text + Environment.NewLine + "Priority: " + project_input.Text + Environment.NewLine + Environment.NewLine + "Log in to BugFixer application and click Fix Bugs to obtain the general bug list and the bugs that are assigned to yourself. Select the appropriate bug and click fix bug" + Environment.NewLine + Environment.NewLine + "Original Poster's contact email: " + replyemail + Environment.NewLine + repoaddress);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.Send(mm);

                MessageBox.Show("Your bug has been submitted and an Email is sent to the assigned user");
                input_clear();
                this.Close();

            }

        }
    }
}
