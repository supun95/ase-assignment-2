﻿namespace BugFixer
{
    partial class List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bug_filt_list = new System.Windows.Forms.ListBox();
            this.bug_req_fix = new System.Windows.Forms.ListBox();
            this.my_bugs = new System.Windows.Forms.ListBox();
            this.sev_type = new System.Windows.Forms.ComboBox();
            this.member = new System.Windows.Forms.TextBox();
            this.severity = new System.Windows.Forms.TextBox();
            this.priority = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.TextBox();
            this.os = new System.Windows.Forms.TextBox();
            this.date = new System.Windows.Forms.TextBox();
            this.bug_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.fix_bug = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(484, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(383, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "List of Bugs";
            // 
            // bug_filt_list
            // 
            this.bug_filt_list.FormattingEnabled = true;
            this.bug_filt_list.ItemHeight = 16;
            this.bug_filt_list.Location = new System.Drawing.Point(34, 159);
            this.bug_filt_list.Name = "bug_filt_list";
            this.bug_filt_list.ScrollAlwaysVisible = true;
            this.bug_filt_list.Size = new System.Drawing.Size(291, 308);
            this.bug_filt_list.TabIndex = 1;
            this.bug_filt_list.SelectedIndexChanged += new System.EventHandler(this.bug_filt_list_SelectedIndexChanged);
            // 
            // bug_req_fix
            // 
            this.bug_req_fix.FormattingEnabled = true;
            this.bug_req_fix.ItemHeight = 16;
            this.bug_req_fix.Location = new System.Drawing.Point(332, 159);
            this.bug_req_fix.Name = "bug_req_fix";
            this.bug_req_fix.ScrollAlwaysVisible = true;
            this.bug_req_fix.Size = new System.Drawing.Size(291, 308);
            this.bug_req_fix.TabIndex = 2;
            this.bug_req_fix.SelectedIndexChanged += new System.EventHandler(this.bug_req_fix_SelectedIndexChanged);
            // 
            // my_bugs
            // 
            this.my_bugs.FormattingEnabled = true;
            this.my_bugs.ItemHeight = 16;
            this.my_bugs.Location = new System.Drawing.Point(632, 159);
            this.my_bugs.Name = "my_bugs";
            this.my_bugs.ScrollAlwaysVisible = true;
            this.my_bugs.Size = new System.Drawing.Size(291, 308);
            this.my_bugs.TabIndex = 3;
            this.my_bugs.SelectedIndexChanged += new System.EventHandler(this.my_bugs_SelectedIndexChanged);
            // 
            // sev_type
            // 
            this.sev_type.FormattingEnabled = true;
            this.sev_type.Location = new System.Drawing.Point(34, 129);
            this.sev_type.Name = "sev_type";
            this.sev_type.Size = new System.Drawing.Size(291, 24);
            this.sev_type.TabIndex = 4;
            this.sev_type.SelectedIndexChanged += new System.EventHandler(this.sev_type_SelectedIndexChanged);
            // 
            // member
            // 
            this.member.Location = new System.Drawing.Point(1072, 188);
            this.member.Name = "member";
            this.member.Size = new System.Drawing.Size(163, 22);
            this.member.TabIndex = 5;
            // 
            // severity
            // 
            this.severity.Location = new System.Drawing.Point(1072, 229);
            this.severity.Name = "severity";
            this.severity.Size = new System.Drawing.Size(163, 22);
            this.severity.TabIndex = 6;
            // 
            // priority
            // 
            this.priority.Location = new System.Drawing.Point(1072, 275);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(163, 22);
            this.priority.TabIndex = 7;
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(1072, 321);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(163, 22);
            this.code.TabIndex = 8;
            // 
            // os
            // 
            this.os.Location = new System.Drawing.Point(1072, 366);
            this.os.Name = "os";
            this.os.Size = new System.Drawing.Size(163, 22);
            this.os.TabIndex = 9;
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(1072, 411);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(163, 22);
            this.date.TabIndex = 10;
            // 
            // bug_name
            // 
            this.bug_name.Location = new System.Drawing.Point(1072, 100);
            this.bug_name.Name = "bug_name";
            this.bug_name.Size = new System.Drawing.Size(163, 22);
            this.bug_name.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(982, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Bug Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(936, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Member";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(936, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Severity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(936, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Priority";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(936, 321);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Code";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(936, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Operating System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(936, 416);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Date Posted";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 17);
            this.label9.TabIndex = 19;
            this.label9.Text = "Filter";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(403, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 17);
            this.label10.TabIndex = 20;
            this.label10.Text = "Bugs Require Fixing";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(724, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "My Priorities";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(1130, 130);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 36);
            this.button1.TabIndex = 22;
            this.button1.Text = "Bug Audit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fix_bug
            // 
            this.fix_bug.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.fix_bug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fix_bug.Location = new System.Drawing.Point(539, 505);
            this.fix_bug.Name = "fix_bug";
            this.fix_bug.Size = new System.Drawing.Size(207, 32);
            this.fix_bug.TabIndex = 23;
            this.fix_bug.Text = "Fix the Bug";
            this.fix_bug.UseVisualStyleBackColor = false;
            this.fix_bug.Click += new System.EventHandler(this.fix_bug_Click);
            // 
            // List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1259, 574);
            this.Controls.Add(this.fix_bug);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bug_name);
            this.Controls.Add(this.date);
            this.Controls.Add(this.os);
            this.Controls.Add(this.code);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.severity);
            this.Controls.Add(this.member);
            this.Controls.Add(this.sev_type);
            this.Controls.Add(this.my_bugs);
            this.Controls.Add(this.bug_req_fix);
            this.Controls.Add(this.bug_filt_list);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.Name = "List";
            this.Text = "List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox bug_filt_list;
        private System.Windows.Forms.ListBox bug_req_fix;
        private System.Windows.Forms.ListBox my_bugs;
        private System.Windows.Forms.ComboBox sev_type;
        private System.Windows.Forms.TextBox member;
        private System.Windows.Forms.TextBox severity;
        private System.Windows.Forms.TextBox priority;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox os;
        private System.Windows.Forms.TextBox date;
        private System.Windows.Forms.TextBox bug_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button fix_bug;
    }
}