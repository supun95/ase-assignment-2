﻿namespace BugFixer
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Signup_Submit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.firstname_input = new System.Windows.Forms.TextBox();
            this.surname_input = new System.Windows.Forms.TextBox();
            this.username_input = new System.Windows.Forms.TextBox();
            this.Email_input = new System.Windows.Forms.TextBox();
            this.Password_input = new System.Windows.Forms.TextBox();
            this.retypePassword_input = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label1.Location = new System.Drawing.Point(227, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sign Up Form";
            // 
            // Signup_Submit
            // 
            this.Signup_Submit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Signup_Submit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Signup_Submit.Location = new System.Drawing.Point(312, 371);
            this.Signup_Submit.Name = "Signup_Submit";
            this.Signup_Submit.Size = new System.Drawing.Size(179, 57);
            this.Signup_Submit.TabIndex = 1;
            this.Signup_Submit.Text = "Submit User";
            this.Signup_Submit.UseVisualStyleBackColor = false;
            this.Signup_Submit.Click += new System.EventHandler(this.Signup_Submit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label2.Location = new System.Drawing.Point(210, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "First Name";
            // 
            // firstname_input
            // 
            this.firstname_input.Location = new System.Drawing.Point(343, 121);
            this.firstname_input.Name = "firstname_input";
            this.firstname_input.Size = new System.Drawing.Size(264, 22);
            this.firstname_input.TabIndex = 3;
            // 
            // surname_input
            // 
            this.surname_input.Location = new System.Drawing.Point(343, 159);
            this.surname_input.Name = "surname_input";
            this.surname_input.Size = new System.Drawing.Size(264, 22);
            this.surname_input.TabIndex = 4;
            // 
            // username_input
            // 
            this.username_input.Location = new System.Drawing.Point(343, 198);
            this.username_input.Name = "username_input";
            this.username_input.Size = new System.Drawing.Size(264, 22);
            this.username_input.TabIndex = 5;
            // 
            // Email_input
            // 
            this.Email_input.Location = new System.Drawing.Point(343, 238);
            this.Email_input.Name = "Email_input";
            this.Email_input.Size = new System.Drawing.Size(264, 22);
            this.Email_input.TabIndex = 6;
            // 
            // Password_input
            // 
            this.Password_input.Location = new System.Drawing.Point(343, 275);
            this.Password_input.Name = "Password_input";
            this.Password_input.Size = new System.Drawing.Size(264, 22);
            this.Password_input.TabIndex = 7;
            // 
            // retypePassword_input
            // 
            this.retypePassword_input.Location = new System.Drawing.Point(343, 315);
            this.retypePassword_input.Name = "retypePassword_input";
            this.retypePassword_input.Size = new System.Drawing.Size(264, 22);
            this.retypePassword_input.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label3.Location = new System.Drawing.Point(210, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Surname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label4.Location = new System.Drawing.Point(210, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label5.Location = new System.Drawing.Point(210, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label6.Location = new System.Drawing.Point(210, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label7.Location = new System.Drawing.Point(202, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Retype-Password";
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(789, 462);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.retypePassword_input);
            this.Controls.Add(this.Password_input);
            this.Controls.Add(this.Email_input);
            this.Controls.Add(this.username_input);
            this.Controls.Add(this.surname_input);
            this.Controls.Add(this.firstname_input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Signup_Submit);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.SlateBlue;
            this.Name = "SignUp";
            this.Text = "SignUp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Signup_Submit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox firstname_input;
        private System.Windows.Forms.TextBox surname_input;
        private System.Windows.Forms.TextBox username_input;
        private System.Windows.Forms.TextBox Email_input;
        private System.Windows.Forms.TextBox Password_input;
        private System.Windows.Forms.TextBox retypePassword_input;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}