﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace BugFixer
{
    public partial class SignUp : Form
    {
        /// <summary>
        /// Relevant methods are initialised when the form loads
        /// </summary>
        public SignUp()
        {
            InitializeComponent();
            database_connection();
            Password_input.PasswordChar = '*';
            retypePassword_input.PasswordChar = '*';
        }

        /// <summary>
        /// initiates sqp connection
        /// </summary>
        SqlConnection mySqlConnection;

        /// <summary>
        ///  connection to the database is established
        /// </summary>
        public void database_connection()
        {
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\BugDatabase.mdf";
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection.Open();
        }

        /// <summary>
        /// textboxes are cleared once the values are added
        /// </summary>
        public void clear_text_boxes()
        {
            firstname_input.Text = surname_input.Text = username_input.Text = Email_input.Text = Password_input.Text = retypePassword_input.Text = "";
        }

        /// <summary>
        /// users password will be hashed using mdf5 hash in order to secure user privacy
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Md5PasswordHash(string text)
        {
            MD5 md5_password_hash = new MD5CryptoServiceProvider();

            md5_password_hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5_password_hash.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// check whether the username already exists in the database and checks for null values. Error messages are displayed accodingly
        /// </summary>
        /// <returns></returns>
        public bool check_input_data()
        {
            bool returnvalue = true;
            String check_existing_users = "SELECT COUNT(*) FROM users WHERE user_name = '" + username_input.Text + "'";
            SqlCommand cmdcheckuser = new SqlCommand(check_existing_users, mySqlConnection);
            int userCount = (int)cmdcheckuser.ExecuteScalar();

            if (userCount > 0)
            {
                MessageBox.Show("Error: User already exists! Please choose an alternative username.");
                returnvalue = false;

            }

            if (
                string.IsNullOrEmpty(firstname_input.Text)
                || string.IsNullOrEmpty(surname_input.Text)
                || string.IsNullOrEmpty(username_input.Text)
                || string.IsNullOrEmpty(Email_input.Text)
                || string.IsNullOrEmpty(Password_input.Text)
                || string.IsNullOrEmpty(retypePassword_input.Text))
            {
                MessageBox.Show("Error: Please check your credentials");
                returnvalue = false;
            }

            if (Password_input.Text != retypePassword_input.Text)
            {
                MessageBox.Show("Error: Your Passwords do not match");
                returnvalue = false;

            }

            return (returnvalue);

        }

        /// <summary>
        /// prepares data in oder to add them into the database
        /// </summary>
        /// <param name="first_name"></param>
        /// <param name="surname"></param>
        /// <param name="user_name"></param>
        /// <param name="email_address"></param>
        /// <param name="password"></param>
        /// <param name="commandString"></param>
        public void insert_user_data(String first_name, String surname, String user_name, String email_address, String password, String commandString)
        {

            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@first_name", first_name);
                cmdInsert.Parameters.AddWithValue("@surname", surname);
                cmdInsert.Parameters.AddWithValue("@user_name", user_name);
                cmdInsert.Parameters.AddWithValue("@user_email", email_address);
                cmdInsert.Parameters.AddWithValue("@user_password", password);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// adds data into the database when user clicks the signup button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Signup_Submit_Click(object sender, EventArgs e)
        {
            if (check_input_data())
            {
                string hashed_password = Md5PasswordHash(Password_input.Text);
                String command_String = "INSERT INTO users(first_name, surname, user_name, user_email, user_password) VALUES (@first_name, @surname, @user_name, @user_email, @user_password)";
                insert_user_data(firstname_input.Text, surname_input.Text, username_input.Text, Email_input.Text, hashed_password, command_String);
                clear_text_boxes();
                MessageBox.Show("New user information added to the System");
                this.Close();

            }
        }
    }
}
