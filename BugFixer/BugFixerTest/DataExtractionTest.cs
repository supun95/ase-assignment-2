﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class DataExtractionTest
    {
        /// <summary>
        /// Tests whether infodisplay() extracts data from the database
        /// </summary>
        [TestMethod]
        public void myprojectsdataextraction()
        {
            //arrange
            string user = "user";
            MyProjects form = new MyProjects(user);

            try
            {
                //act
                form.infoDisplay();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether commitlist() extracts data from the database
        /// </summary>
        [TestMethod]
        public void bugauditdataextraction()
        {
            //arrange
            string user = "user";
            string bug = "bug";
            BugAudit form = new BugAudit(bug, user);

            try
            {
                //act
                form.commitlist();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether buglist_ALL(), bugs_assigned() and severitytype() extracts data from the database
        /// </summary>
        [TestMethod]
        public void listdataextraction()
        {
            //arrange
            string user = "user";
            List form = new List(user);

            try
            {
                //act
                form.buglist_ALL();
                form.bugs_assigned(); 
                form.severitytype();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether Start_form() extracts data from the database
        /// </summary>
        [TestMethod]
        public void fixbugdataextraction()
        {
            //arrange
            string user = "user";
            string bug = "bug";
            FixBug form = new FixBug(bug, user);

            try
            {
                //act
                form.Start_form();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// checks whether infodisplay() extracts data from the database
        /// </summary>
        [TestMethod]
        public void myprojdataextraction()
        {
            //arrange
            string user = "user";
            MyProjects form = new MyProjects(user);

            try
            {
                //act
                form.infoDisplay();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether infodisplay() extracts data from the database
        /// </summary>
        [TestMethod]
        public void myquestionsdataextraction()
        {
            //arrange
            string user = "user";
            MyQuestions form = new MyQuestions(user);

            try
            {
                //act
                form.infodisplay();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether from_start() extracts data from the database
        /// </summary>
        [TestMethod]
        public void bugsoldataextraction()
        {
            //arrange
            string user = "user";
            string bug = "bug";
            BugSolution form = new BugSolution(bug, user);

            try
            {
                //act
                form.from_start(); 
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Tests whether infodisplay() extracts data from the database
        /// </summary>
        [TestMethod]
        public void bugsollistdataextraction()
        {
            //arrange
            string user = "user";
            BugSolList form = new BugSolList(user);

            try
            {
                //act
                form.infodisplay();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
