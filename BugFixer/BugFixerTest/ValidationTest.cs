﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class ValidationTest
    {
        /// <summary>
        /// check whether check_input_data() in SignUp works. this method is similar in all the other forms
        /// </summary>
        [TestMethod]
        public void check_input_data()
        {
            //arrange
            SignUp form = new SignUp();
            //act
            try
            {
                form.check_input_data();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
