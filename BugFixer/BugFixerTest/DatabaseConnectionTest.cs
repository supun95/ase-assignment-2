﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class DatabaseConnectionTest
    {
        /// <summary>
        /// Test to check the validity of the database connection
        /// </summary>
        [TestMethod]
        public void databaseconnectiontest()
        {
            //arrange
            SqlConnection mySqlConnection;
            string connection = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\Advanced Software Engineering\BugFixer\BugFixer\bin\Debug\BugDatabase.mdf;Integrated Security=True;Connect Timeout=30";
            //act          
            try
            {
                mySqlConnection = new SqlConnection(connection);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

    }
}
