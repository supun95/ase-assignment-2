﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class ListItemsTest
    {
        /// <summary>
        /// Test of reportform_projectfill_test() method which is the provision of information to the user through a list box.
        /// </summary>
        [TestMethod]
        public void reportform_listitem_test()
        {
            //arrange
            string user = "user";
            ReportBug form = new ReportBug(user);

            //act
            try
            {
                form.droplistitems();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test of listform_listitem_test() method which is the provision of information to the user through a list box.
        /// </summary>
        [TestMethod]
        public void listform_listitem_test()
        {
            //arrange
            string user = "user";
            List form = new List(user);

            //act
            try
            {
                form.severitytype();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test of bug_status() method which is the provision of information to the user through a list box.
        /// </summary>
        [TestMethod]
        public void bugsolform_listitem_test()
        {
            //arrange
            string user = "user";
            string bug = "bug";
            BugSolution form = new BugSolution(user, bug);

            //act
            try
            {
                form.bug_status();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
