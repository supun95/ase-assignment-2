﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class FormLoadTest
    {
        /// <summary>
        /// Test to check the opening of bugaudit form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void bugauditformload()
        {
            //arrange
            string username = "user";
            string bugname = "bug";
            //act          
            try
            {
                BugAudit newForm = new BugAudit(username, bugname);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of bugsollist form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void bugsollistformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                BugSolList newForm = new BugSolList(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of bugsolution form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void bugsolutiontformload()
        {
            //arrange
            string username = "user";
            string bugname = "bug";
            //act          
            try
            {
                BugSolution newForm = new BugSolution(username, bugname);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of fixbugform with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void Fixbugformload()
        {
            //arrange
            string username = "user";
            string bugname = "bug";
            //act          
            try
            {
                FixBug newForm = new FixBug(username, bugname);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of List form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void listformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                List newForm = new List(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of MainHub form 
        /// </summary>
        [TestMethod]
        public void MainHubformload()
        {
            //act          
            try
            {
                MainHub newForm = new MainHub();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of MyProjects form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void myprojectsformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                MyProjects newForm = new MyProjects(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of MyQuestions form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void myquestionsformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                MyQuestions newForm = new MyQuestions(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of NewProject form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void newprojectformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                NewProject newForm = new NewProject(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of reportbug form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void reporbugformload()
        {
            //arrange
            string username = "user";
            //act          
            try
            {
                ReportBug newForm = new ReportBug(username);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Test to check the opening of signup form with its acceptance of passed values
        /// </summary>
        [TestMethod]
        public void signupformload()
        {
            //act          
            try
            {
                SignUp newForm = new SignUp();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }


    }
}
