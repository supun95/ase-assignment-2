﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using BugFixer;
using System.Data.SqlClient;

namespace BugFixerTest
{
    [TestClass]
    public class PasswordTest
    {
        /// <summary>
        /// test of the md5 password hashing for the sign up form
        /// </summary>
        [TestMethod]
        public void SignUpPassCheck()
        {
            // arrange
            string pass_string = "password";
            string expected_hashvalue = "5f4dcc3b5aa765d61d8327deb882cf99";
            //act
            String actual = SignUp.Md5PasswordHash(pass_string);
            //assert
            Assert.AreEqual(expected_hashvalue, actual);
        }

        /// <summary>
        /// test of the md5 password hashing for the login form 
        /// </summary>
        [TestMethod]
        public void LoginPassCheck()
        {
            // arrange
            string pass_string = "password";
            string expected_hashvalue = "5f4dcc3b5aa765d61d8327deb882cf99";
            //act
            String actual = MainHub.Md5PasswordHash(pass_string);
            //assert
            Assert.AreEqual(expected_hashvalue, actual);
        }

    }
}
